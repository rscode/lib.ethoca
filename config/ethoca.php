<?php

use Ratespecial\Ethoca\ServiceProvider\EthocaServiceProvider;

return [
    /*
     * This is not your login to the web portal.  Within the web portal, there is an option to "Change API Password".  That is where you
     * determine your API username and password.
     */
    'username' => env('RATESPECIAL_ETHOCA_USERNAME', ''),
    'password' => env('RATESPECIAL_ETHOCA_PASSWORD', ''),

    'env' => env('RATESPECIAL_ETHOCA_ENV', EthocaServiceProvider::LIVE_SERVICE),
];
