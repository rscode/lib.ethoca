# Mastercard Ethoca Alerts PHP SDK

## Contents

* [Introduction](#introduction)
  * [Ethoca Documentation](#ethoca-documentation)
* [Installation](#installation)
* [Usage](#usage)
    * [Laravel usage](#laravel-usage)
    * [Non-Laravel usage](#non-laravel-usage)

## Introduction

PHP library built from Ethoca provided WSDL file. Used [wsdltophp/packagegenerator](https://github.com/WsdlToPhp/PackageGenerator) to
generate PHP files, then modified to make usage easier.

### Ethoca Documentation

https://www.ethoca.com/ethoca-alerts

https://developer.mastercard.com/product/ethoca-alerts-for-merchants

https://developer.mastercard.com/ethoca-alerts-for-merchants/documentation

## Installation

```
composer require ratespecial/ethoca
```

## Usage

### Laravel usage

The `ethoca.php` config file will be merged into the app.  Only two env variables need be
set:

```dotenv
RATESPECIAL_ETHOCA_USERNAME=
RATESPECIAL_ETHOCA_PASSWORD=
```

`EthocaServiceProvider` will be auto discovered.  There are two services bound to the container:

* `EthocaService` aimed at Ethoca production
* `EthocaService` aimed at Ethoca staging

By default, injecting `EthocaService` will use the production configuration (specified in the config).  You can set your default Ethoca
environment by setting the `RATESPECIAL_ETHOCA_ENV` variable.
following:

```dotenv
#RATESPECIAL_ETHOCA_ENV=ethoca-live
RATESPECIAL_ETHOCA_ENV=ethoca-sandbox
```

You can also just create sandbox or production instance where needed

```php
public function process()
{
    // sandbox instance
    /** @var EthocaService $service */
    $service = App::make(EthocaServiceProvider::SANDBOX_SERVICE)
    
    // production instance
    /** @var EthocaService $service */
    $service = App::make(EthocaServiceProvider::LIVE_SERVICE)
}
```

You can inject `EthocaService` anywhere it is needed.  Again by default, it will use the production configuration.

```php
use Ratespecial\Ethoca\Alerts\EnumType\Outcome;
use Ratespecial\Ethoca\Alerts\EnumType\Refunded;
use Ratespecial\Ethoca\Alerts\EthocaDate;
use Ratespecial\Ethoca\Alerts\EthocaService;
use Ratespecial\Ethoca\Alerts\StructType\AlertUpdatesType;
use Ratespecial\Ethoca\Alerts\StructType\AlertUpdateType;
use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsUpdateRequest;

public function process(EthocaService $ethocaService)
{
    $upd = new AlertUpdatesType([
        new AlertUpdateType(
            ethocaID: 'AA1K4MD0YT8HSHPV1I7IB4P63',
            outcome: Outcome::VALUE_ACCOUNT_SUSPENDED,
            refunded: Refunded::VALUE_NOT_SETTLED,
            actionTimestamp: EthocaDate::format(),
            comments: 'whoopsie',
        ),
        new AlertUpdateType(
            ethocaID: 'AA1K4MD0YT8HSHPV1I7IB4P64',
            outcome: Outcome::VALUE_ACCOUNT_SUSPENDED,
            refunded: Refunded::VALUE_NOT_SETTLED,
            actionTimestamp: EthocaDate::format(),
            comments: 'whoopsie',
        ),
    ]);
    $req = new Ethoca360AlertsUpdateRequest();
    $req->setAlertUpdates($upd);
    
    $res = $ethocaService->alertUpdate($req);
}
```

### Non-Laravel usage

You'll need to build the service yourself, configuring the Soap options.  Ensure it's pointing [WSDL file](wsdl/EthocaAlerts%20-%20Sandbox.wsdl).

```php
public function process()
{
    // Build service
    $options = [
        SoapClientInterface::WSDL_URL                => realpath('whatever_path_manipulation/vendor/ratespecial/ethoca/wsdl/EthocaAlerts - Sandbox.wsdl'),
        SoapClientInterface::WSDL_CLASSMAP           => ClassMap::get(),
        SoapClientInterface::WSDL_LOCATION           => 'https://sandbox.ethocaweb.com/axis2/services/EthocaServices',
        SoapClientInterface::WSDL_CONNECTION_TIMEOUT => 140,
    ];

    $s = new EthocaService($options);

    $s->setAuthentication('your-username', 'your-password');
    
    // Use service
    $upd = new AlertUpdatesType([
        new AlertUpdateType(
            ethocaID: 'AA1K4MD0YT8HSHPV1I7IB4P63',
            outcome: Outcome::VALUE_ACCOUNT_SUSPENDED,
            refunded: Refunded::VALUE_NOT_SETTLED,
            actionTimestamp: EthocaDate::format(),
            comments: 'whoopsie',
        ),
    ]);
    $req = new Ethoca360AlertsUpdateRequest();
    $req->setAlertUpdates($upd);
    
    $res = $s->alertUpdate($req);
}
```
