<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlertsType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AlertsType extends AbstractStructBase
{
    /**
     * The Alert
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     *
     * @var AlertType[]
     */
    protected ?array $Alert = null;

    /**
     * Constructor method for AlertsType
     *
     * @param AlertType[] $alert
     * @uses AlertsType::setAlert()
     */
    public function __construct(?array $alert = null)
    {
        $this
            ->setAlert($alert);
    }

    /**
     * This method is responsible for validating the value(s) passed to the setAlert method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlert method
     * This has to validate that each item contained by the array match the itemType constraint
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAlertForArrayConstraintFromSetAlert(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $alertsTypeAlertItem) {
            // validation for constraint: itemType
            if (!$alertsTypeAlertItem instanceof AlertType) {
                $invalidValues[] = is_object($alertsTypeAlertItem) ? get_class($alertsTypeAlertItem) : sprintf(
                    '%s(%s)',
                    gettype($alertsTypeAlertItem),
                    var_export($alertsTypeAlertItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The Alert property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\AlertType, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get Alert value
     *
     * @return AlertType[]
     */
    public function getAlert(): ?array
    {
        return $this->Alert;
    }

    /**
     * Set Alert value
     *
     * @param AlertType[] $alert
     * @return AlertsType
     * @throws InvalidArgumentException
     */
    public function setAlert(?array $alert = null): self
    {
        // validation for constraint: array
        if ('' !== ($alertArrayErrorMessage = self::validateAlertForArrayConstraintFromSetAlert($alert))) {
            throw new InvalidArgumentException($alertArrayErrorMessage, __LINE__);
        }
        $this->Alert = $alert;

        return $this;
    }

    /**
     * Add item to Alert value
     *
     * @param AlertType $item
     * @return AlertsType
     * @throws InvalidArgumentException
     */
    public function addToAlert(AlertType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AlertType) {
            throw new InvalidArgumentException(sprintf(
                'The Alert property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\AlertType, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->Alert[] = $item;

        return $this;
    }
}
