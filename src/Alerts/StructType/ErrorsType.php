<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ErrorsType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ErrorsType extends AbstractStructBase
{
    /**
     * The Error
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     *
     * @var ErrorType[]
     */
    protected ?array $Error = null;

    /**
     * Constructor method for ErrorsType
     *
     * @param ErrorType[] $error
     * @uses ErrorsType::setError()
     */
    public function __construct(?array $error = null)
    {
        $this
            ->setError($error);
    }

    /**
     * This method is responsible for validating the value(s) passed to the setError method
     * This method is willingly generated in order to preserve the one-line inline validation within the setError method
     * This has to validate that each item contained by the array match the itemType constraint
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateErrorForArrayConstraintFromSetError(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $errorsTypeErrorItem) {
            // validation for constraint: itemType
            if (!$errorsTypeErrorItem instanceof ErrorType) {
                $invalidValues[] = is_object($errorsTypeErrorItem) ? get_class($errorsTypeErrorItem) : sprintf(
                    '%s(%s)',
                    gettype($errorsTypeErrorItem),
                    var_export($errorsTypeErrorItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The Error property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\ErrorType, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get Error value
     *
     * @return ErrorType[]
     */
    public function getError(): ?array
    {
        return $this->Error;
    }

    /**
     * Set Error value
     *
     * @param ErrorType[] $error
     * @return ErrorsType
     * @throws InvalidArgumentException
     */
    public function setError(?array $error = null): self
    {
        // validation for constraint: array
        if ('' !== ($errorArrayErrorMessage = self::validateErrorForArrayConstraintFromSetError($error))) {
            throw new InvalidArgumentException($errorArrayErrorMessage, __LINE__);
        }
        $this->Error = $error;

        return $this;
    }

    /**
     * Add item to Error value
     *
     * @param ErrorType $item
     * @return ErrorsType
     * @throws InvalidArgumentException
     */
    public function addToError(ErrorType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof ErrorType) {
            throw new InvalidArgumentException(sprintf(
                'The Error property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\ErrorType, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->Error[] = $item;

        return $this;
    }
}
