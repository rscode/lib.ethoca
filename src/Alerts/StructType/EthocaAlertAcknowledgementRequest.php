<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EthocaAlertAcknowledgementRequestType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class EthocaAlertAcknowledgementRequest extends AbstractStructBase implements EthocaRequestInterface
{
    /**
     * The AlertAcknowledgements
     * Meta information extracted from the WSDL
     * - maxOccurs: 1
     * - minOccurs: 1
     *
     * @var AlertAcknowledgementRequestType
     */
    protected AlertAcknowledgementRequestType $AlertAcknowledgements;
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 30
     * - minLength: 5
     *
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 35
     * - minLength: 8
     *
     * @var string|null
     */
    protected ?string $Password = null;

    public function __construct(
        AlertAcknowledgementRequestType $alertAcknowledgements,
        ?string $username = null,
        ?string $password = null
    ) {
        $this
            ->setAlertAcknowledgements($alertAcknowledgements)
            ->setUsername($username)
            ->setPassword($password);
    }

    /**
     * Get AlertAcknowledgements value
     *
     * @return AlertAcknowledgementRequestType
     */
    public function getAlertAcknowledgements(): AlertAcknowledgementRequestType
    {
        return $this->AlertAcknowledgements;
    }

    /**
     * Set AlertAcknowledgements value
     *
     * @param AlertAcknowledgementRequestType $alertAcknowledgements
     * @return EthocaAlertAcknowledgementRequest
     */
    public function setAlertAcknowledgements(AlertAcknowledgementRequestType $alertAcknowledgements): self
    {
        $this->AlertAcknowledgements = $alertAcknowledgements;

        return $this;
    }

    /**
     * Get Username value
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->Username;
    }

    /**
     * Set Username value
     *
     * @param string $username
     * @return EthocaAlertAcknowledgementRequest
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($username, true),
                gettype($username)
            ), __LINE__);
        }
        // validation for constraint: maxLength(30)
        if (!is_null($username) && mb_strlen((string)$username) > 30) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 30',
                mb_strlen((string)$username)
            ), __LINE__);
        }
        // validation for constraint: minLength(5)
        if (!is_null($username) && mb_strlen((string)$username) < 5) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 5',
                mb_strlen((string)$username)
            ), __LINE__);
        }
        $this->Username = $username;

        return $this;
    }

    /**
     * Get Password value
     *
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->Password;
    }

    /**
     * Set Password value
     *
     * @param string $password
     * @return EthocaAlertAcknowledgementRequest
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($password, true),
                gettype($password)
            ), __LINE__);
        }
        // validation for constraint: maxLength(35)
        if (!is_null($password) && mb_strlen((string)$password) > 35) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 35',
                mb_strlen((string)$password)
            ), __LINE__);
        }
        // validation for constraint: minLength(8)
        if (!is_null($password) && mb_strlen((string)$password) < 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 8',
                mb_strlen((string)$password)
            ), __LINE__);
        }
        $this->Password = $password;

        return $this;
    }
}
