<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use Ratespecial\Ethoca\Alerts\EnumType\Status;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Ethoca360AlertsResponseType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Ethoca360AlertsResponse extends AbstractStructBase
{
    /**
     * The NumberOfAlerts
     * Meta information extracted from the WSDL
     * - minOccurs: 1
     *
     * @var int
     */
    protected int $NumberOfAlerts;
    /**
     * The majorCode
     * Meta information extracted from the WSDL
     * - use: required
     *
     * @var int
     */
    protected int $majorCode;
    /**
     * The Status
     *
     * @var string|null
     */
    protected ?string $Status = null;
    /**
     * The Alerts
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var AlertsType|null
     */
    protected ?AlertsType $Alerts = null;
    /**
     * The Errors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var ErrorsType|null
     */
    protected ?ErrorsType $Errors = null;

    /**
     * Constructor method for Ethoca360AlertsResponseType
     *
     * @param int        $numberOfAlerts
     * @param int        $majorCode
     * @param string     $status
     * @param AlertsType $alerts
     * @param ErrorsType $errors
     * @uses Ethoca360AlertsResponse::setNumberOfAlerts()
     * @uses Ethoca360AlertsResponse::setMajorCode()
     * @uses Ethoca360AlertsResponse::setStatus()
     * @uses Ethoca360AlertsResponse::setAlerts()
     * @uses Ethoca360AlertsResponse::setErrors()
     */
    public function __construct(
        int $numberOfAlerts,
        int $majorCode,
        ?string $status = null,
        ?AlertsType $alerts = null,
        ?ErrorsType $errors = null
    ) {
        $this
            ->setNumberOfAlerts($numberOfAlerts)
            ->setMajorCode($majorCode)
            ->setStatus($status)
            ->setAlerts($alerts)
            ->setErrors($errors);
    }

    /**
     * Get NumberOfAlerts value
     *
     * @return int
     */
    public function getNumberOfAlerts(): int
    {
        return $this->NumberOfAlerts;
    }

    /**
     * Set NumberOfAlerts value
     *
     * @param int $numberOfAlerts
     * @return Ethoca360AlertsResponse
     */
    public function setNumberOfAlerts(int $numberOfAlerts): self
    {
        // validation for constraint: int
        if (!is_null($numberOfAlerts) && !(is_int($numberOfAlerts) || ctype_digit($numberOfAlerts))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($numberOfAlerts, true),
                gettype($numberOfAlerts)
            ), __LINE__);
        }
        $this->NumberOfAlerts = $numberOfAlerts;

        return $this;
    }

    /**
     * Get majorCode value
     *
     * @return int
     */
    public function getMajorCode(): int
    {
        return $this->majorCode;
    }

    /**
     * Set majorCode value
     *
     * @param int $majorCode
     * @return Ethoca360AlertsResponse
     */
    public function setMajorCode(int $majorCode): self
    {
        // validation for constraint: int
        if (!is_null($majorCode) && !(is_int($majorCode) || ctype_digit($majorCode))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($majorCode, true),
                gettype($majorCode)
            ), __LINE__);
        }
        $this->majorCode = $majorCode;

        return $this;
    }

    /**
     * Get Status value
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }

    /**
     * Set Status value
     *
     * @param string $status
     * @return Ethoca360AlertsResponse
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Status::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Status::valueIsValid()
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!Status::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Status',
                is_array($status) ? implode(', ', $status) : var_export($status, true),
                implode(', ', Status::getValidValues())
            ), __LINE__);
        }
        $this->Status = $status;

        return $this;
    }

    /**
     * Get Alerts value
     *
     * @return AlertsType|null
     */
    public function getAlerts(): ?AlertsType
    {
        return $this->Alerts;
    }

    /**
     * Set Alerts value
     *
     * @param AlertsType $alerts
     * @return Ethoca360AlertsResponse
     */
    public function setAlerts(?AlertsType $alerts = null): self
    {
        $this->Alerts = $alerts;

        return $this;
    }

    /**
     * Get Errors value
     *
     * @return ErrorsType|null
     */
    public function getErrors(): ?ErrorsType
    {
        return $this->Errors;
    }

    /**
     * Set Errors value
     *
     * @param ErrorsType $errors
     * @return Ethoca360AlertsResponse
     */
    public function setErrors(?ErrorsType $errors = null): self
    {
        $this->Errors = $errors;

        return $this;
    }
}
