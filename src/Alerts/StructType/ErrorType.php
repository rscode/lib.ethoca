<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for ErrorType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class ErrorType extends AbstractStructBase
{
    /**
     * The _
     *
     * @var string|null
     */
    protected ?string $_ = null;
    /**
     * The code
     * Meta information extracted from the WSDL
     * - use: optional
     *
     * @var int|null
     */
    protected ?int $code = null;

    /**
     * Constructor method for ErrorType
     *
     * @param string $_
     * @param int    $code
     * @uses ErrorType::set_()
     * @uses ErrorType::setCode()
     */
    public function __construct(?string $_ = null, ?int $code = null)
    {
        $this
            ->set_($_)
            ->setCode($code);
    }

    /**
     * Get _ value
     *
     * @return string|null
     */
    public function get_(): ?string
    {
        return $this->_;
    }

    /**
     * Set _ value
     *
     * @param string $_
     * @return ErrorType
     */
    public function set_(?string $_ = null): self
    {
        // validation for constraint: string
        if (!is_null($_) && !is_string($_)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($_, true),
                gettype($_)
            ), __LINE__);
        }
        $this->_ = $_;

        return $this;
    }

    /**
     * Get code value
     *
     * @return int|null
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * Set code value
     *
     * @param int $code
     * @return ErrorType
     */
    public function setCode(?int $code = null): self
    {
        // validation for constraint: int
        if (!is_null($code) && !(is_int($code) || ctype_digit($code))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($code, true),
                gettype($code)
            ), __LINE__);
        }
        $this->code = $code;

        return $this;
    }
}
