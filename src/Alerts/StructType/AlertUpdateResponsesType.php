<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlertUpdateResponsesType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AlertUpdateResponsesType extends AbstractStructBase
{
    /**
     * The AlertUpdateResponse
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     *
     * @var AlertUpdateResponseType[]
     */
    protected ?array $AlertUpdateResponse = null;

    /**
     * Constructor method for AlertUpdateResponsesType
     *
     * @param AlertUpdateResponseType[] $alertUpdateResponse
     * @uses AlertUpdateResponsesType::setAlertUpdateResponse()
     */
    public function __construct(?array $alertUpdateResponse = null)
    {
        $this
            ->setAlertUpdateResponse($alertUpdateResponse);
    }

    /**
     * This method is responsible for validating the value(s) passed to the setAlertUpdateResponse method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlertUpdateResponse method
     * This has to validate that each item contained by the array match the itemType constraint
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAlertUpdateResponseForArrayConstraintFromSetAlertUpdateResponse(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $alertUpdateResponsesTypeAlertUpdateResponseItem) {
            // validation for constraint: itemType
            if (!$alertUpdateResponsesTypeAlertUpdateResponseItem instanceof AlertUpdateResponseType) {
                $invalidValues[] = is_object($alertUpdateResponsesTypeAlertUpdateResponseItem) ? get_class($alertUpdateResponsesTypeAlertUpdateResponseItem) : sprintf(
                    '%s(%s)',
                    gettype($alertUpdateResponsesTypeAlertUpdateResponseItem),
                    var_export($alertUpdateResponsesTypeAlertUpdateResponseItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The AlertUpdateResponse property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\AlertUpdateResponseType, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get AlertUpdateResponse value
     *
     * @return AlertUpdateResponseType[]
     */
    public function getAlertUpdateResponse(): ?array
    {
        return $this->AlertUpdateResponse;
    }

    /**
     * Set AlertUpdateResponse value
     *
     * @param AlertUpdateResponseType[] $alertUpdateResponse
     * @return AlertUpdateResponsesType
     * @throws InvalidArgumentException
     */
    public function setAlertUpdateResponse(?array $alertUpdateResponse = null): self
    {
        // validation for constraint: array
        if ('' !== ($alertUpdateResponseArrayErrorMessage = self::validateAlertUpdateResponseForArrayConstraintFromSetAlertUpdateResponse($alertUpdateResponse))) {
            throw new InvalidArgumentException($alertUpdateResponseArrayErrorMessage, __LINE__);
        }
        $this->AlertUpdateResponse = $alertUpdateResponse;

        return $this;
    }

    /**
     * Add item to AlertUpdateResponse value
     *
     * @param AlertUpdateResponseType $item
     * @return AlertUpdateResponsesType
     * @throws InvalidArgumentException
     */
    public function addToAlertUpdateResponse(AlertUpdateResponseType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof AlertUpdateResponseType) {
            throw new InvalidArgumentException(sprintf(
                'The AlertUpdateResponse property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\AlertUpdateResponseType, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->AlertUpdateResponse[] = $item;

        return $this;
    }
}
