<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use Ratespecial\Ethoca\Alerts\EnumType\InitiatedBy;
use Ratespecial\Ethoca\Alerts\EnumType\Is3DSecure;
use Ratespecial\Ethoca\Alerts\EnumType\TransactionType;
use Ratespecial\Ethoca\Alerts\EnumType\Type;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlertType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AlertType extends AbstractStructBase
{
    /**
     * The EthocaID
     * Meta information extracted from the WSDL
     * - base: string
     * - length: 25
     *
     * @var string|null
     */
    protected ?string $EthocaID = null;
    /**
     * The Type
     *
     * @var string|null
     */
    protected ?string $Type = null;
    /**
     * The AlertTimestamp
     * Meta information extracted from the WSDL
     * - documentation: YYYY-MM-DDThh:mm:ss.sss
     * - base: string
     * - pattern: [12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?
     *
     * @var string|null
     */
    protected ?string $AlertTimestamp = null;
    /**
     * The Age
     *
     * @var string|null
     */
    protected ?string $Age = null;
    /**
     * The Issuer
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 100
     *
     * @var string|null
     */
    protected ?string $Issuer = null;
    /**
     * The CardNumber
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 19
     * - minLength: 13
     *
     * @var string|null
     */
    protected ?string $CardNumber = null;
    /**
     * The CardBIN
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 6
     * - minLength: 1
     * - minOccurs: 0
     * - pattern: [0-9]{1,6}
     *
     * @var string|null
     */
    protected ?string $CardBIN = null;
    /**
     * The CardBIN8
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 8
     * - minLength: 8
     * - minOccurs: 0
     * - pattern: [0-9]{1,8}
     *
     * @var string|null
     */
    protected ?string $CardBIN8 = null;
    /**
     * The CardLast4
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 4
     * - minLength: 1
     * - minOccurs: 0
     * - pattern: [0-9]{1,4}
     *
     * @var string|null
     */
    protected ?string $CardLast4 = null;
    /**
     * The ARN
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 64
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ARN = null;
    /**
     * The TransactionTimestamp
     * Meta information extracted from the WSDL
     * - documentation: YYYY-MM-DDThh:mm:ss.sss
     * - base: string
     * - pattern: [12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?
     *
     * @var string|null
     */
    protected ?string $TransactionTimestamp = null;
    /**
     * The MerchantDescriptor
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 50
     *
     * @var string|null
     */
    protected ?string $MerchantDescriptor = null;
    /**
     * The MemberID
     *
     * @var int|null
     */
    protected ?int $MemberID = null;
    /**
     * The MCC
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 4
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $MCC = null;
    /**
     * The Amount
     * Meta information extracted from the WSDL
     * - base: decimal
     * - fractionDigits: 3
     * - minInclusive: 0
     * - totalDigits: 11
     *
     * @var float|null
     */
    protected ?float $Amount = null;
    /**
     * The Currency
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 3
     * - minLength: 3
     *
     * @var string|null
     */
    protected ?string $Currency = null;
    /**
     * The TransactionType
     *
     * @var string|null
     */
    protected ?string $TransactionType = null;
    /**
     * The InitiatedBy
     *
     * @var string|null
     */
    protected ?string $InitiatedBy = null;
    /**
     * The Is3DSecure
     *
     * @var string|null
     */
    protected ?string $Is3DSecure = null;
    /**
     * The Source
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 50
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $Source = null;
    /**
     * The AuthCode
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 8
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $AuthCode = null;
    /**
     * The MerchantMemberName
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 60
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $MerchantMemberName = null;
    /**
     * The TransactionId
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 64
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $TransactionId = null;
    /**
     * The ChargebackReasonCode
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 30
     * - minLength: 0
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ChargebackReasonCode = null;
    /**
     * The ChargebackAmount
     * Meta information extracted from the WSDL
     * - base: decimal
     * - fractionDigits: 3
     * - minInclusive: 0
     * - minOccurs: 0
     * - totalDigits: 11
     *
     * @var float|null
     */
    protected ?float $ChargebackAmount = null;
    /**
     * The ChargebackCurrency
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 3
     * - minLength: 3
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $ChargebackCurrency = null;

    /**
     * Constructor method for AlertType
     *
     * @param string $ethocaID
     * @param string $type
     * @param string $alertTimestamp
     * @param string $age
     * @param string $issuer
     * @param string $cardNumber
     * @param string $cardBIN
     * @param string $cardBIN8
     * @param string $cardLast4
     * @param string $aRN
     * @param string $transactionTimestamp
     * @param string $merchantDescriptor
     * @param int    $memberID
     * @param string $mCC
     * @param float  $amount
     * @param string $currency
     * @param string $transactionType
     * @param string $initiatedBy
     * @param string $is3DSecure
     * @param string $source
     * @param string $authCode
     * @param string $merchantMemberName
     * @param string $transactionId
     * @param string $chargebackReasonCode
     * @param float  $chargebackAmount
     * @param string $chargebackCurrency
     * @uses AlertType::setEthocaID()
     * @uses AlertType::setType()
     * @uses AlertType::setAlertTimestamp()
     * @uses AlertType::setAge()
     * @uses AlertType::setIssuer()
     * @uses AlertType::setCardNumber()
     * @uses AlertType::setCardBIN()
     * @uses AlertType::setCardBIN8()
     * @uses AlertType::setCardLast4()
     * @uses AlertType::setARN()
     * @uses AlertType::setTransactionTimestamp()
     * @uses AlertType::setMerchantDescriptor()
     * @uses AlertType::setMemberID()
     * @uses AlertType::setMCC()
     * @uses AlertType::setAmount()
     * @uses AlertType::setCurrency()
     * @uses AlertType::setTransactionType()
     * @uses AlertType::setInitiatedBy()
     * @uses AlertType::setIs3DSecure()
     * @uses AlertType::setSource()
     * @uses AlertType::setAuthCode()
     * @uses AlertType::setMerchantMemberName()
     * @uses AlertType::setTransactionId()
     * @uses AlertType::setChargebackReasonCode()
     * @uses AlertType::setChargebackAmount()
     * @uses AlertType::setChargebackCurrency()
     */
    public function __construct(
        ?string $ethocaID = null,
        ?string $type = null,
        ?string $alertTimestamp = null,
        ?string $age = null,
        ?string $issuer = null,
        ?string $cardNumber = null,
        ?string $cardBIN = null,
        ?string $cardBIN8 = null,
        ?string $cardLast4 = null,
        ?string $aRN = null,
        ?string $transactionTimestamp = null,
        ?string $merchantDescriptor = null,
        ?int $memberID = null,
        ?string $mCC = null,
        ?float $amount = null,
        ?string $currency = null,
        ?string $transactionType = null,
        ?string $initiatedBy = null,
        ?string $is3DSecure = null,
        ?string $source = null,
        ?string $authCode = null,
        ?string $merchantMemberName = null,
        ?string $transactionId = null,
        ?string $chargebackReasonCode = null,
        ?float $chargebackAmount = null,
        ?string $chargebackCurrency = null
    ) {
        $this
            ->setEthocaID($ethocaID)
            ->setType($type)
            ->setAlertTimestamp($alertTimestamp)
            ->setAge($age)
            ->setIssuer($issuer)
            ->setCardNumber($cardNumber)
            ->setCardBIN($cardBIN)
            ->setCardBIN8($cardBIN8)
            ->setCardLast4($cardLast4)
            ->setARN($aRN)
            ->setTransactionTimestamp($transactionTimestamp)
            ->setMerchantDescriptor($merchantDescriptor)
            ->setMemberID($memberID)
            ->setMCC($mCC)
            ->setAmount($amount)
            ->setCurrency($currency)
            ->setTransactionType($transactionType)
            ->setInitiatedBy($initiatedBy)
            ->setIs3DSecure($is3DSecure)
            ->setSource($source)
            ->setAuthCode($authCode)
            ->setMerchantMemberName($merchantMemberName)
            ->setTransactionId($transactionId)
            ->setChargebackReasonCode($chargebackReasonCode)
            ->setChargebackAmount($chargebackAmount)
            ->setChargebackCurrency($chargebackCurrency);
    }

    /**
     * Get EthocaID value
     *
     * @return string|null
     */
    public function getEthocaID(): ?string
    {
        return $this->EthocaID;
    }

    /**
     * Set EthocaID value
     *
     * @param string $ethocaID
     * @return AlertType
     */
    public function setEthocaID(?string $ethocaID = null): self
    {
        // validation for constraint: string
        if (!is_null($ethocaID) && !is_string($ethocaID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ethocaID, true),
                gettype($ethocaID)
            ), __LINE__);
        }
        // validation for constraint: length(25)
        if (!is_null($ethocaID) && mb_strlen((string)$ethocaID) !== 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 25',
                mb_strlen((string)$ethocaID)
            ), __LINE__);
        }
        $this->EthocaID = $ethocaID;

        return $this;
    }

    /**
     * Get Type value
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->Type;
    }

    /**
     * Set Type value
     *
     * @param string $type
     * @return AlertType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Type::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Type::valueIsValid()
     */
    public function setType(?string $type = null): self
    {
        // validation for constraint: enumeration
        if (!Type::valueIsValid($type)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Type',
                is_array($type) ? implode(', ', $type) : var_export($type, true),
                implode(', ', Type::getValidValues())
            ), __LINE__);
        }
        $this->Type = $type;

        return $this;
    }

    /**
     * Get AlertTimestamp value
     *
     * @return string|null
     */
    public function getAlertTimestamp(): ?string
    {
        return $this->AlertTimestamp;
    }

    /**
     * Set AlertTimestamp value
     *
     * @param string $alertTimestamp
     * @return AlertType
     */
    public function setAlertTimestamp(?string $alertTimestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($alertTimestamp) && !is_string($alertTimestamp)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($alertTimestamp, true),
                gettype($alertTimestamp)
            ), __LINE__);
        }
        // validation for constraint: pattern([12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?)
        if (
            !is_null($alertTimestamp) && !preg_match(
                '/[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                (string)$alertTimestamp
            )
        ) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                var_export($alertTimestamp, true)
            ), __LINE__);
        }
        $this->AlertTimestamp = $alertTimestamp;

        return $this;
    }

    /**
     * Get Age value
     *
     * @return string|null
     */
    public function getAge(): ?string
    {
        return $this->Age;
    }

    /**
     * Set Age value
     *
     * @param string $age
     * @return AlertType
     */
    public function setAge(?string $age = null): self
    {
        // validation for constraint: string
        if (!is_null($age) && !is_string($age)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($age, true),
                gettype($age)
            ), __LINE__);
        }
        $this->Age = $age;

        return $this;
    }

    /**
     * Get Issuer value
     *
     * @return string|null
     */
    public function getIssuer(): ?string
    {
        return $this->Issuer;
    }

    /**
     * Set Issuer value
     *
     * @param string $issuer
     * @return AlertType
     */
    public function setIssuer(?string $issuer = null): self
    {
        // validation for constraint: string
        if (!is_null($issuer) && !is_string($issuer)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($issuer, true),
                gettype($issuer)
            ), __LINE__);
        }
        // validation for constraint: maxLength(100)
        if (!is_null($issuer) && mb_strlen((string)$issuer) > 100) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 100',
                mb_strlen((string)$issuer)
            ), __LINE__);
        }
        $this->Issuer = $issuer;

        return $this;
    }

    /**
     * Get CardNumber value
     *
     * @return string|null
     */
    public function getCardNumber(): ?string
    {
        return $this->CardNumber;
    }

    /**
     * Set CardNumber value
     *
     * @param string $cardNumber
     * @return AlertType
     */
    public function setCardNumber(?string $cardNumber = null): self
    {
        // validation for constraint: string
        if (!is_null($cardNumber) && !is_string($cardNumber)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($cardNumber, true),
                gettype($cardNumber)
            ), __LINE__);
        }
        // validation for constraint: maxLength(19)
        if (!is_null($cardNumber) && mb_strlen((string)$cardNumber) > 19) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 19',
                mb_strlen((string)$cardNumber)
            ), __LINE__);
        }
        // validation for constraint: minLength(13)
        if (!is_null($cardNumber) && mb_strlen((string)$cardNumber) < 13) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 13',
                mb_strlen((string)$cardNumber)
            ), __LINE__);
        }
        $this->CardNumber = $cardNumber;

        return $this;
    }

    /**
     * Get CardBIN value
     *
     * @return string|null
     */
    public function getCardBIN(): ?string
    {
        return $this->CardBIN;
    }

    /**
     * Set CardBIN value
     *
     * @param string $cardBIN
     * @return AlertType
     */
    public function setCardBIN(?string $cardBIN = null): self
    {
        // validation for constraint: string
        if (!is_null($cardBIN) && !is_string($cardBIN)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($cardBIN, true),
                gettype($cardBIN)
            ), __LINE__);
        }
        // validation for constraint: maxLength(6)
        if (!is_null($cardBIN) && mb_strlen((string)$cardBIN) > 6) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 6',
                mb_strlen((string)$cardBIN)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($cardBIN) && mb_strlen((string)$cardBIN) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$cardBIN)
            ), __LINE__);
        }
        // validation for constraint: pattern([0-9]{1,6})
        if (!is_null($cardBIN) && !preg_match('/[0-9]{1,6}/', (string)$cardBIN)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[0-9]{1,6}/',
                var_export($cardBIN, true)
            ), __LINE__);
        }
        $this->CardBIN = $cardBIN;

        return $this;
    }

    /**
     * Get CardBIN8 value
     *
     * @return string|null
     */
    public function getCardBIN8(): ?string
    {
        return $this->CardBIN8;
    }

    /**
     * Set CardBIN8 value
     *
     * @param string $cardBIN8
     * @return AlertType
     */
    public function setCardBIN8(?string $cardBIN8 = null): self
    {
        // validation for constraint: string
        if (!is_null($cardBIN8) && !is_string($cardBIN8)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($cardBIN8, true),
                gettype($cardBIN8)
            ), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($cardBIN8) && mb_strlen((string)$cardBIN8) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8',
                mb_strlen((string)$cardBIN8)
            ), __LINE__);
        }
        // validation for constraint: minLength(8)
        if (!is_null($cardBIN8) && mb_strlen((string)$cardBIN8) < 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 8',
                mb_strlen((string)$cardBIN8)
            ), __LINE__);
        }
        // validation for constraint: pattern([0-9]{1,8})
        if (!is_null($cardBIN8) && !preg_match('/[0-9]{1,8}/', (string)$cardBIN8)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[0-9]{1,8}/',
                var_export($cardBIN8, true)
            ), __LINE__);
        }
        $this->CardBIN8 = $cardBIN8;

        return $this;
    }

    /**
     * Get CardLast4 value
     *
     * @return string|null
     */
    public function getCardLast4(): ?string
    {
        return $this->CardLast4;
    }

    /**
     * Set CardLast4 value
     *
     * @param string $cardLast4
     * @return AlertType
     */
    public function setCardLast4(?string $cardLast4 = null): self
    {
        // validation for constraint: string
        if (!is_null($cardLast4) && !is_string($cardLast4)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($cardLast4, true),
                gettype($cardLast4)
            ), __LINE__);
        }
        // validation for constraint: maxLength(4)
        if (!is_null($cardLast4) && mb_strlen((string)$cardLast4) > 4) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 4',
                mb_strlen((string)$cardLast4)
            ), __LINE__);
        }
        // validation for constraint: minLength(1)
        if (!is_null($cardLast4) && mb_strlen((string)$cardLast4) < 1) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 1',
                mb_strlen((string)$cardLast4)
            ), __LINE__);
        }
        // validation for constraint: pattern([0-9]{1,4})
        if (!is_null($cardLast4) && !preg_match('/[0-9]{1,4}/', (string)$cardLast4)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[0-9]{1,4}/',
                var_export($cardLast4, true)
            ), __LINE__);
        }
        $this->CardLast4 = $cardLast4;

        return $this;
    }

    /**
     * Get ARN value
     *
     * @return string|null
     */
    public function getARN(): ?string
    {
        return $this->ARN;
    }

    /**
     * Set ARN value
     *
     * @param string $aRN
     * @return AlertType
     */
    public function setARN(?string $aRN = null): self
    {
        // validation for constraint: string
        if (!is_null($aRN) && !is_string($aRN)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($aRN, true),
                gettype($aRN)
            ), __LINE__);
        }
        // validation for constraint: maxLength(64)
        if (!is_null($aRN) && mb_strlen((string)$aRN) > 64) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 64',
                mb_strlen((string)$aRN)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($aRN) && mb_strlen((string)$aRN) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$aRN)
            ), __LINE__);
        }
        $this->ARN = $aRN;

        return $this;
    }

    /**
     * Get TransactionTimestamp value
     *
     * @return string|null
     */
    public function getTransactionTimestamp(): ?string
    {
        return $this->TransactionTimestamp;
    }

    /**
     * Set TransactionTimestamp value
     *
     * @param string $transactionTimestamp
     * @return AlertType
     */
    public function setTransactionTimestamp(?string $transactionTimestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($transactionTimestamp) && !is_string($transactionTimestamp)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($transactionTimestamp, true),
                gettype($transactionTimestamp)
            ), __LINE__);
        }
        // validation for constraint: pattern([12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?)
        if (
            !is_null($transactionTimestamp) && !preg_match(
                '/[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                (string)$transactionTimestamp
            )
        ) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                var_export($transactionTimestamp, true)
            ), __LINE__);
        }
        $this->TransactionTimestamp = $transactionTimestamp;

        return $this;
    }

    /**
     * Get MerchantDescriptor value
     *
     * @return string|null
     */
    public function getMerchantDescriptor(): ?string
    {
        return $this->MerchantDescriptor;
    }

    /**
     * Set MerchantDescriptor value
     *
     * @param string $merchantDescriptor
     * @return AlertType
     */
    public function setMerchantDescriptor(?string $merchantDescriptor = null): self
    {
        // validation for constraint: string
        if (!is_null($merchantDescriptor) && !is_string($merchantDescriptor)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($merchantDescriptor, true),
                gettype($merchantDescriptor)
            ), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($merchantDescriptor) && mb_strlen((string)$merchantDescriptor) > 50) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50',
                mb_strlen((string)$merchantDescriptor)
            ), __LINE__);
        }
        $this->MerchantDescriptor = $merchantDescriptor;

        return $this;
    }

    /**
     * Get MemberID value
     *
     * @return int|null
     */
    public function getMemberID(): ?int
    {
        return $this->MemberID;
    }

    /**
     * Set MemberID value
     *
     * @param int $memberID
     * @return AlertType
     */
    public function setMemberID(?int $memberID = null): self
    {
        // validation for constraint: int
        if (!is_null($memberID) && !(is_int($memberID) || ctype_digit($memberID))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($memberID, true),
                gettype($memberID)
            ), __LINE__);
        }
        $this->MemberID = $memberID;

        return $this;
    }

    /**
     * Get MCC value
     *
     * @return string|null
     */
    public function getMCC(): ?string
    {
        return $this->MCC;
    }

    /**
     * Set MCC value
     *
     * @param string $mCC
     * @return AlertType
     */
    public function setMCC(?string $mCC = null): self
    {
        // validation for constraint: string
        if (!is_null($mCC) && !is_string($mCC)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($mCC, true),
                gettype($mCC)
            ), __LINE__);
        }
        // validation for constraint: maxLength(4)
        if (!is_null($mCC) && mb_strlen((string)$mCC) > 4) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 4',
                mb_strlen((string)$mCC)
            ), __LINE__);
        }
        $this->MCC = $mCC;

        return $this;
    }

    /**
     * Get Amount value
     *
     * @return float|null
     */
    public function getAmount(): ?float
    {
        return $this->Amount;
    }

    /**
     * Set Amount value
     *
     * @param float $amount
     * @return AlertType
     */
    public function setAmount(?float $amount = null): self
    {
        // validation for constraint: float
        if (!is_null($amount) && !(is_float($amount) || is_numeric($amount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a float value, %s given',
                var_export($amount, true),
                gettype($amount)
            ), __LINE__);
        }
        // validation for constraint: fractionDigits(3)
        if (
            !is_null($amount) && mb_strlen(mb_substr(
                (string)$amount,
                false !== mb_strpos((string)$amount, '.') ? mb_strpos((string)$amount, '.') + 1 : mb_strlen((string)$amount)
            )) > 3
        ) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must at most contain 3 fraction digits, %d given',
                var_export($amount, true),
                mb_strlen(mb_substr((string)$amount, mb_strpos((string)$amount, '.') + 1))
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($amount) && $amount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($amount, true)
            ), __LINE__);
        }
        // validation for constraint: totalDigits(11)
        if (!is_null($amount) && mb_strlen(preg_replace('/(\D)/', '', (string)$amount)) > 11) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must use at most 11 digits, "%d" given',
                var_export($amount, true),
                mb_strlen(preg_replace('/(\D)/', '', (string)$amount))
            ), __LINE__);
        }
        $this->Amount = $amount;

        return $this;
    }

    /**
     * Get Currency value
     *
     * @return string|null
     */
    public function getCurrency(): ?string
    {
        return $this->Currency;
    }

    /**
     * Set Currency value
     *
     * @param string $currency
     * @return AlertType
     */
    public function setCurrency(?string $currency = null): self
    {
        // validation for constraint: string
        if (!is_null($currency) && !is_string($currency)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($currency, true),
                gettype($currency)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($currency) && mb_strlen((string)$currency) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$currency)
            ), __LINE__);
        }
        // validation for constraint: minLength(3)
        if (!is_null($currency) && mb_strlen((string)$currency) < 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 3',
                mb_strlen((string)$currency)
            ), __LINE__);
        }
        $this->Currency = $currency;

        return $this;
    }

    /**
     * Get TransactionType value
     *
     * @return string|null
     */
    public function getTransactionType(): ?string
    {
        return $this->TransactionType;
    }

    /**
     * Set TransactionType value
     *
     * @param string $transactionType
     * @return AlertType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\TransactionType::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\TransactionType::valueIsValid()
     */
    public function setTransactionType(?string $transactionType = null): self
    {
        // validation for constraint: enumeration
        if (!TransactionType::valueIsValid($transactionType)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\TransactionType',
                is_array($transactionType) ? implode(', ', $transactionType) : var_export($transactionType, true),
                implode(', ', TransactionType::getValidValues())
            ), __LINE__);
        }
        $this->TransactionType = $transactionType;

        return $this;
    }

    /**
     * Get InitiatedBy value
     *
     * @return string|null
     */
    public function getInitiatedBy(): ?string
    {
        return $this->InitiatedBy;
    }

    /**
     * Set InitiatedBy value
     *
     * @param string $initiatedBy
     * @return AlertType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\InitiatedBy::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\InitiatedBy::valueIsValid()
     */
    public function setInitiatedBy(?string $initiatedBy = null): self
    {
        // validation for constraint: enumeration
        if (!InitiatedBy::valueIsValid($initiatedBy)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\InitiatedBy',
                is_array($initiatedBy) ? implode(', ', $initiatedBy) : var_export($initiatedBy, true),
                implode(', ', InitiatedBy::getValidValues())
            ), __LINE__);
        }
        $this->InitiatedBy = $initiatedBy;

        return $this;
    }

    /**
     * Get Is3DSecure value
     *
     * @return string|null
     */
    public function getIs3DSecure(): ?string
    {
        return $this->Is3DSecure;
    }

    /**
     * Set Is3DSecure value
     *
     * @param string $is3DSecure
     * @return AlertType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Is3DSecure::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Is3DSecure::valueIsValid()
     */
    public function setIs3DSecure(?string $is3DSecure = null): self
    {
        // validation for constraint: enumeration
        if (!Is3DSecure::valueIsValid($is3DSecure)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Is3DSecure',
                is_array($is3DSecure) ? implode(', ', $is3DSecure) : var_export($is3DSecure, true),
                implode(', ', Is3DSecure::getValidValues())
            ), __LINE__);
        }
        $this->Is3DSecure = $is3DSecure;

        return $this;
    }

    /**
     * Get Source value
     *
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->Source;
    }

    /**
     * Set Source value
     *
     * @param string $source
     * @return AlertType
     */
    public function setSource(?string $source = null): self
    {
        // validation for constraint: string
        if (!is_null($source) && !is_string($source)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($source, true),
                gettype($source)
            ), __LINE__);
        }
        // validation for constraint: maxLength(50)
        if (!is_null($source) && mb_strlen((string)$source) > 50) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 50',
                mb_strlen((string)$source)
            ), __LINE__);
        }
        $this->Source = $source;

        return $this;
    }

    /**
     * Get AuthCode value
     *
     * @return string|null
     */
    public function getAuthCode(): ?string
    {
        return $this->AuthCode;
    }

    /**
     * Set AuthCode value
     *
     * @param string $authCode
     * @return AlertType
     */
    public function setAuthCode(?string $authCode = null): self
    {
        // validation for constraint: string
        if (!is_null($authCode) && !is_string($authCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($authCode, true),
                gettype($authCode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(8)
        if (!is_null($authCode) && mb_strlen((string)$authCode) > 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 8',
                mb_strlen((string)$authCode)
            ), __LINE__);
        }
        $this->AuthCode = $authCode;

        return $this;
    }

    /**
     * Get MerchantMemberName value
     *
     * @return string|null
     */
    public function getMerchantMemberName(): ?string
    {
        return $this->MerchantMemberName;
    }

    /**
     * Set MerchantMemberName value
     *
     * @param string $merchantMemberName
     * @return AlertType
     */
    public function setMerchantMemberName(?string $merchantMemberName = null): self
    {
        // validation for constraint: string
        if (!is_null($merchantMemberName) && !is_string($merchantMemberName)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($merchantMemberName, true),
                gettype($merchantMemberName)
            ), __LINE__);
        }
        // validation for constraint: maxLength(60)
        if (!is_null($merchantMemberName) && mb_strlen((string)$merchantMemberName) > 60) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 60',
                mb_strlen((string)$merchantMemberName)
            ), __LINE__);
        }
        $this->MerchantMemberName = $merchantMemberName;

        return $this;
    }

    /**
     * Get TransactionId value
     *
     * @return string|null
     */
    public function getTransactionId(): ?string
    {
        return $this->TransactionId;
    }

    /**
     * Set TransactionId value
     *
     * @param string $transactionId
     * @return AlertType
     */
    public function setTransactionId(?string $transactionId = null): self
    {
        // validation for constraint: string
        if (!is_null($transactionId) && !is_string($transactionId)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($transactionId, true),
                gettype($transactionId)
            ), __LINE__);
        }
        // validation for constraint: maxLength(64)
        if (!is_null($transactionId) && mb_strlen((string)$transactionId) > 64) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 64',
                mb_strlen((string)$transactionId)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($transactionId) && mb_strlen((string)$transactionId) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$transactionId)
            ), __LINE__);
        }
        $this->TransactionId = $transactionId;

        return $this;
    }

    /**
     * Get ChargebackReasonCode value
     *
     * @return string|null
     */
    public function getChargebackReasonCode(): ?string
    {
        return $this->ChargebackReasonCode;
    }

    /**
     * Set ChargebackReasonCode value
     *
     * @param string $chargebackReasonCode
     * @return AlertType
     */
    public function setChargebackReasonCode(?string $chargebackReasonCode = null): self
    {
        // validation for constraint: string
        if (!is_null($chargebackReasonCode) && !is_string($chargebackReasonCode)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($chargebackReasonCode, true),
                gettype($chargebackReasonCode)
            ), __LINE__);
        }
        // validation for constraint: maxLength(30)
        if (!is_null($chargebackReasonCode) && mb_strlen((string)$chargebackReasonCode) > 30) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 30',
                mb_strlen((string)$chargebackReasonCode)
            ), __LINE__);
        }
        // validation for constraint: minLength
        if (!is_null($chargebackReasonCode) && mb_strlen((string)$chargebackReasonCode) < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 0',
                mb_strlen((string)$chargebackReasonCode)
            ), __LINE__);
        }
        $this->ChargebackReasonCode = $chargebackReasonCode;

        return $this;
    }

    /**
     * Get ChargebackAmount value
     *
     * @return float|null
     */
    public function getChargebackAmount(): ?float
    {
        return $this->ChargebackAmount;
    }

    /**
     * Set ChargebackAmount value
     *
     * @param float $chargebackAmount
     * @return AlertType
     */
    public function setChargebackAmount(?float $chargebackAmount = null): self
    {
        // validation for constraint: float
        if (!is_null($chargebackAmount) && !(is_float($chargebackAmount) || is_numeric($chargebackAmount))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a float value, %s given',
                var_export($chargebackAmount, true),
                gettype($chargebackAmount)
            ), __LINE__);
        }
        // validation for constraint: fractionDigits(3)
        if (
            !is_null($chargebackAmount) && mb_strlen(mb_substr(
                (string)$chargebackAmount,
                false !== mb_strpos((string)$chargebackAmount, '.') ? mb_strpos(
                    (string)$chargebackAmount,
                    '.'
                ) + 1 : mb_strlen((string)$chargebackAmount)
            )) > 3
        ) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must at most contain 3 fraction digits, %d given',
                var_export($chargebackAmount, true),
                mb_strlen(mb_substr((string)$chargebackAmount, mb_strpos((string)$chargebackAmount, '.') + 1))
            ), __LINE__);
        }
        // validation for constraint: minInclusive
        if (!is_null($chargebackAmount) && $chargebackAmount < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($chargebackAmount, true)
            ), __LINE__);
        }
        // validation for constraint: totalDigits(11)
        if (!is_null($chargebackAmount) && mb_strlen(preg_replace('/(\D)/', '', (string)$chargebackAmount)) > 11) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must use at most 11 digits, "%d" given',
                var_export($chargebackAmount, true),
                mb_strlen(preg_replace('/(\D)/', '', (string)$chargebackAmount))
            ), __LINE__);
        }
        $this->ChargebackAmount = $chargebackAmount;

        return $this;
    }

    /**
     * Get ChargebackCurrency value
     *
     * @return string|null
     */
    public function getChargebackCurrency(): ?string
    {
        return $this->ChargebackCurrency;
    }

    /**
     * Set ChargebackCurrency value
     *
     * @param string $chargebackCurrency
     * @return AlertType
     */
    public function setChargebackCurrency(?string $chargebackCurrency = null): self
    {
        // validation for constraint: string
        if (!is_null($chargebackCurrency) && !is_string($chargebackCurrency)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($chargebackCurrency, true),
                gettype($chargebackCurrency)
            ), __LINE__);
        }
        // validation for constraint: maxLength(3)
        if (!is_null($chargebackCurrency) && mb_strlen((string)$chargebackCurrency) > 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 3',
                mb_strlen((string)$chargebackCurrency)
            ), __LINE__);
        }
        // validation for constraint: minLength(3)
        if (!is_null($chargebackCurrency) && mb_strlen((string)$chargebackCurrency) < 3) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 3',
                mb_strlen((string)$chargebackCurrency)
            ), __LINE__);
        }
        $this->ChargebackCurrency = $chargebackCurrency;

        return $this;
    }
}
