<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlertUpdateResponseType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AlertUpdateResponseType extends AbstractStructBase
{
    /**
     * The ethocaID
     * Meta information extracted from the WSDL
     * - use: required
     *
     * @var string
     */
    protected string $ethocaID;
    /**
     * The status
     * Meta information extracted from the WSDL
     * - use: required
     *
     * @var string
     */
    protected string $status;
    /**
     * The Errors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var ErrorsType|null
     */
    protected ?ErrorsType $Errors = null;

    public function __construct(string $ethocaID, string $status, ?ErrorsType $errors = null)
    {
        $this
            ->setEthocaID($ethocaID)
            ->setStatus($status)
            ->setErrors($errors);
    }

    /**
     * Get ethocaID value
     *
     * @return string
     */
    public function getEthocaID(): string
    {
        return $this->ethocaID;
    }

    /**
     * Set ethocaID value
     *
     * @param string $ethocaID
     * @return AlertUpdateResponseType
     */
    public function setEthocaID(string $ethocaID): self
    {
        // validation for constraint: string
        if (!is_null($ethocaID) && !is_string($ethocaID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ethocaID, true),
                gettype($ethocaID)
            ), __LINE__);
        }
        $this->ethocaID = $ethocaID;

        return $this;
    }

    /**
     * Get status value
     *
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Set status value
     *
     * @param string $status
     * @return AlertUpdateResponseType
     */
    public function setStatus(string $status): self
    {
        // validation for constraint: string
        if (!is_null($status) && !is_string($status)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($status, true),
                gettype($status)
            ), __LINE__);
        }
        $this->status = $status;

        return $this;
    }

    /**
     * Get Errors value
     *
     * @return ErrorsType|null
     */
    public function getErrors(): ?ErrorsType
    {
        return $this->Errors;
    }

    /**
     * Set Errors value
     *
     * @param ErrorsType $errors
     * @return AlertUpdateResponseType
     */
    public function setErrors(?ErrorsType $errors = null): self
    {
        $this->Errors = $errors;

        return $this;
    }
}
