<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Ethoca360AlertsUpdateResponseType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Ethoca360AlertsUpdateResponse extends AbstractStructBase
{
    /**
     * The majorCode
     * Meta information extracted from the WSDL
     * - use: required
     *
     * @var int
     */
    protected int $majorCode;
    /**
     * The AlertUpdateResponses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var AlertUpdateResponsesType|null
     */
    protected ?AlertUpdateResponsesType $AlertUpdateResponses = null;
    /**
     * The Errors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var ErrorsType|null
     */
    protected ?ErrorsType $Errors = null;

    public function __construct(
        int $majorCode,
        ?AlertUpdateResponsesType $alertUpdateResponses = null,
        ?ErrorsType $errors = null
    ) {
        $this
            ->setMajorCode($majorCode)
            ->setAlertUpdateResponses($alertUpdateResponses)
            ->setErrors($errors);
    }

    /**
     * Get majorCode value
     *
     * @return int
     */
    public function getMajorCode(): int
    {
        return $this->majorCode;
    }

    /**
     * Set majorCode value
     *
     * @param int $majorCode
     * @return Ethoca360AlertsUpdateResponse
     */
    public function setMajorCode(int $majorCode): self
    {
        // validation for constraint: int
        if (!is_null($majorCode) && !(is_int($majorCode) || ctype_digit($majorCode))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($majorCode, true),
                gettype($majorCode)
            ), __LINE__);
        }
        $this->majorCode = $majorCode;

        return $this;
    }

    /**
     * Get AlertUpdateResponses value
     *
     * @return AlertUpdateResponsesType|null
     */
    public function getAlertUpdateResponses(): ?AlertUpdateResponsesType
    {
        return $this->AlertUpdateResponses;
    }

    /**
     * Set AlertUpdateResponses value
     *
     * @param AlertUpdateResponsesType $alertUpdateResponses
     * @return Ethoca360AlertsUpdateResponse
     */
    public function setAlertUpdateResponses(?AlertUpdateResponsesType $alertUpdateResponses = null): self
    {
        $this->AlertUpdateResponses = $alertUpdateResponses;

        return $this;
    }

    /**
     * Get Errors value
     *
     * @return ErrorsType|null
     */
    public function getErrors(): ?ErrorsType
    {
        return $this->Errors;
    }

    /**
     * Set Errors value
     *
     * @param ErrorsType $errors
     * @return Ethoca360AlertsUpdateResponse
     */
    public function setErrors(?ErrorsType $errors = null): self
    {
        $this->Errors = $errors;

        return $this;
    }
}
