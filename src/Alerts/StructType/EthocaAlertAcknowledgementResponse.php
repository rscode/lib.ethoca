<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EthocaAlertAcknowledgementResponseType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class EthocaAlertAcknowledgementResponse extends AbstractStructBase
{
    /**
     * The majorCode
     * Meta information extracted from the WSDL
     * - use: required
     *
     * @var int
     */
    protected int $majorCode;
    /**
     * The AcknowledgementResponses
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var AlertUpdateResponsesType|null
     */
    protected ?AlertUpdateResponsesType $AcknowledgementResponses = null;
    /**
     * The Errors
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var ErrorsType|null
     */
    protected ?ErrorsType $Errors = null;

    /**
     * Constructor method for EthocaAlertAcknowledgementResponseType
     *
     * @param int                      $majorCode
     * @param AlertUpdateResponsesType $acknowledgementResponses
     * @param ErrorsType               $errors
     * @uses EthocaAlertAcknowledgementResponse::setMajorCode()
     * @uses EthocaAlertAcknowledgementResponse::setAcknowledgementResponses()
     * @uses EthocaAlertAcknowledgementResponse::setErrors()
     */
    public function __construct(
        int $majorCode,
        ?AlertUpdateResponsesType $acknowledgementResponses = null,
        ?ErrorsType $errors = null
    ) {
        $this
            ->setMajorCode($majorCode)
            ->setAcknowledgementResponses($acknowledgementResponses)
            ->setErrors($errors);
    }

    /**
     * Get majorCode value
     *
     * @return int
     */
    public function getMajorCode(): int
    {
        return $this->majorCode;
    }

    /**
     * Set majorCode value
     *
     * @param int $majorCode
     * @return EthocaAlertAcknowledgementResponse
     */
    public function setMajorCode(int $majorCode): self
    {
        // validation for constraint: int
        if (!is_null($majorCode) && !(is_int($majorCode) || ctype_digit($majorCode))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide an integer value, %s given',
                var_export($majorCode, true),
                gettype($majorCode)
            ), __LINE__);
        }
        $this->majorCode = $majorCode;

        return $this;
    }

    /**
     * Get AcknowledgementResponses value
     *
     * @return AlertUpdateResponsesType|null
     */
    public function getAcknowledgementResponses(): ?AlertUpdateResponsesType
    {
        return $this->AcknowledgementResponses;
    }

    /**
     * Set AcknowledgementResponses value
     *
     * @param AlertUpdateResponsesType $acknowledgementResponses
     * @return EthocaAlertAcknowledgementResponse
     */
    public function setAcknowledgementResponses(
        ?AlertUpdateResponsesType $acknowledgementResponses = null
    ): self {
        $this->AcknowledgementResponses = $acknowledgementResponses;

        return $this;
    }

    /**
     * Get Errors value
     *
     * @return ErrorsType|null
     */
    public function getErrors(): ?ErrorsType
    {
        return $this->Errors;
    }

    /**
     * Set Errors value
     *
     * @param ErrorsType $errors
     * @return EthocaAlertAcknowledgementResponse
     */
    public function setErrors(?ErrorsType $errors = null): self
    {
        $this->Errors = $errors;

        return $this;
    }
}
