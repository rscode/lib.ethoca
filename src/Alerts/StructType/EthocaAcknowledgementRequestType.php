<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use Ratespecial\Ethoca\Alerts\EnumType\Status;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for EthocaAcknowledgementRequestType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class EthocaAcknowledgementRequestType extends AbstractStructBase
{
    /**
     * The EthocaID
     * Meta information extracted from the WSDL
     * - base: string
     * - length: 25
     *
     * @var string|null
     */
    protected ?string $EthocaID = null;
    /**
     * The Status
     *
     * @var string|null
     */
    protected ?string $Status = null;

    /**
     * Constructor method for EthocaAcknowledgementRequestType
     *
     * @param string $ethocaID
     * @param string $status
     * @uses EthocaAcknowledgementRequestType::setEthocaID()
     * @uses EthocaAcknowledgementRequestType::setStatus()
     */
    public function __construct(?string $ethocaID = null, ?string $status = null)
    {
        $this
            ->setEthocaID($ethocaID)
            ->setStatus($status);
    }

    /**
     * Get EthocaID value
     *
     * @return string|null
     */
    public function getEthocaID(): ?string
    {
        return $this->EthocaID;
    }

    /**
     * Set EthocaID value
     *
     * @param string $ethocaID
     * @return EthocaAcknowledgementRequestType
     */
    public function setEthocaID(?string $ethocaID = null): self
    {
        // validation for constraint: string
        if (!is_null($ethocaID) && !is_string($ethocaID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ethocaID, true),
                gettype($ethocaID)
            ), __LINE__);
        }
        // validation for constraint: length(25)
        if (!is_null($ethocaID) && mb_strlen((string)$ethocaID) !== 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 25',
                mb_strlen((string)$ethocaID)
            ), __LINE__);
        }
        $this->EthocaID = $ethocaID;

        return $this;
    }

    /**
     * Get Status value
     *
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->Status;
    }

    /**
     * Set Status value
     *
     * @param string $status
     * @return EthocaAcknowledgementRequestType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Status::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Status::valueIsValid()
     */
    public function setStatus(?string $status = null): self
    {
        // validation for constraint: enumeration
        if (!Status::valueIsValid($status)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Status',
                is_array($status) ? implode(', ', $status) : var_export($status, true),
                implode(', ', Status::getValidValues())
            ), __LINE__);
        }
        $this->Status = $status;

        return $this;
    }
}
