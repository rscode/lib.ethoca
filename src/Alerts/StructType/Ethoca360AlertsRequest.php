<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for Ethoca360AlertsRequestType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class Ethoca360AlertsRequest extends AbstractStructBase implements EthocaRequestInterface
{
    /**
     * The Username
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 30
     * - minLength: 5
     *
     * @var string|null
     */
    protected ?string $Username = null;
    /**
     * The Password
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 35
     * - minLength: 8
     *
     * @var string|null
     */
    protected ?string $Password = null;
    /**
     * Only issuer_alert and customerdispute_alert are supported.
     *
     * @var string|null AlertType::*
     */
    protected ?string $AlertType = null;
    /**
     * The SearchStartDate
     * Meta information extracted from the WSDL
     * - documentation: YYYY-MM-DD
     * - base: string
     * - minOccurs: 0
     * - pattern: [12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}
     *
     * @var string|null
     */
    protected ?string $SearchStartDate = null;
    /**
     * The SearchEndDate
     * Meta information extracted from the WSDL
     * - documentation: YYYY-MM-DD
     * - base: string
     * - minOccurs: 0
     * - pattern: [12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}
     *
     * @var string|null
     */
    protected ?string $SearchEndDate = null;

    public function __construct(
        ?string $username = null,
        ?string $password = null,
        ?string $alertType = null,
        ?string $searchStartDate = null,
        ?string $searchEndDate = null
    ) {
        $this
            ->setUsername($username)
            ->setPassword($password)
            ->setAlertType($alertType)
            ->setSearchStartDate($searchStartDate)
            ->setSearchEndDate($searchEndDate);
    }

    /**
     * Get Username value
     *
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->Username;
    }

    /**
     * Set Username value
     *
     * @param string $username
     * @return Ethoca360AlertsRequest
     */
    public function setUsername(?string $username = null): self
    {
        // validation for constraint: string
        if (!is_null($username) && !is_string($username)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($username, true),
                gettype($username)
            ), __LINE__);
        }
        // validation for constraint: maxLength(30)
        if (!is_null($username) && mb_strlen((string)$username) > 30) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 30',
                mb_strlen((string)$username)
            ), __LINE__);
        }
        // validation for constraint: minLength(5)
        if (!is_null($username) && mb_strlen((string)$username) < 5) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 5',
                mb_strlen((string)$username)
            ), __LINE__);
        }
        $this->Username = $username;

        return $this;
    }

    /**
     * Get Password value
     *
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->Password;
    }

    /**
     * Set Password value
     *
     * @param string $password
     * @return Ethoca360AlertsRequest
     */
    public function setPassword(?string $password = null): self
    {
        // validation for constraint: string
        if (!is_null($password) && !is_string($password)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($password, true),
                gettype($password)
            ), __LINE__);
        }
        // validation for constraint: maxLength(35)
        if (!is_null($password) && mb_strlen((string)$password) > 35) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 35',
                mb_strlen((string)$password)
            ), __LINE__);
        }
        // validation for constraint: minLength(8)
        if (!is_null($password) && mb_strlen((string)$password) < 8) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be greater than or equal to 8',
                mb_strlen((string)$password)
            ), __LINE__);
        }
        $this->Password = $password;

        return $this;
    }

    public function getAlertType(): ?string
    {
        return $this->AlertType;
    }

    public function setAlertType(?string $alertType = null): self
    {
        $this->AlertType = $alertType;

        return $this;
    }

    /**
     * Get SearchStartDate value
     *
     * @return string|null
     */
    public function getSearchStartDate(): ?string
    {
        return $this->SearchStartDate;
    }

    /**
     * Set SearchStartDate value
     *
     * @param string $searchStartDate
     * @return Ethoca360AlertsRequest
     */
    public function setSearchStartDate(?string $searchStartDate = null): self
    {
        // validation for constraint: string
        if (!is_null($searchStartDate) && !is_string($searchStartDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($searchStartDate, true),
                gettype($searchStartDate)
            ), __LINE__);
        }
        // validation for constraint: pattern([12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2})
        if (!is_null($searchStartDate) && !preg_match('/[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}/', (string)$searchStartDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}/',
                var_export($searchStartDate, true)
            ), __LINE__);
        }
        $this->SearchStartDate = $searchStartDate;

        return $this;
    }

    /**
     * Get SearchEndDate value
     *
     * @return string|null
     */
    public function getSearchEndDate(): ?string
    {
        return $this->SearchEndDate;
    }

    /**
     * Set SearchEndDate value
     *
     * @param string $searchEndDate
     * @return Ethoca360AlertsRequest
     */
    public function setSearchEndDate(?string $searchEndDate = null): self
    {
        // validation for constraint: string
        if (!is_null($searchEndDate) && !is_string($searchEndDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($searchEndDate, true),
                gettype($searchEndDate)
            ), __LINE__);
        }
        // validation for constraint: pattern([12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2})
        if (!is_null($searchEndDate) && !preg_match('/[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}/', (string)$searchEndDate)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}/',
                var_export($searchEndDate, true)
            ), __LINE__);
        }
        $this->SearchEndDate = $searchEndDate;

        return $this;
    }
}
