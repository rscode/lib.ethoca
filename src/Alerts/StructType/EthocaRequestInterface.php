<?php

namespace Ratespecial\Ethoca\Alerts\StructType;

interface EthocaRequestInterface
{
    public function setUsername(?string $username = null): self;
    public function setPassword(?string $password = null): self;
    public function getUsername(): ?string;
    public function getPassword(): ?string;
}
