<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use Ratespecial\Ethoca\Alerts\EnumType\FirstPartyFraud;
use Ratespecial\Ethoca\Alerts\EnumType\Outcome;
use Ratespecial\Ethoca\Alerts\EnumType\Refunded;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * For many of these values, see the matching EnumType
 */
#[\AllowDynamicProperties]
class AlertUpdateType extends AbstractStructBase
{
    /**
     * The EthocaID
     * Meta information extracted from the WSDL
     * - base: string
     * - length: 25
     *
     * @var string|null
     */
    protected ?string $EthocaID = null;
    /**
     * The Outcome
     *
     * @var string|null
     * @see Outcome
     */
    protected ?string $Outcome = null;
    /**
     * The Refunded
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $Refunded = null;
    /**
     * The FirstPartyFraud
     * Meta information extracted from the WSDL
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $FirstPartyFraud = null;
    /**
     * The AmountStopped
     * Meta information extracted from the WSDL
     * - documentation: Element valid only when partially stopped outcome is selected. It is otherwise ignored.
     * - base: decimal
     * - fractionDigits: 3
     * - minInclusive: 0
     * - minOccurs: 0
     * - totalDigits: 11
     *
     * @var float|null
     */
    protected ?float $AmountStopped = null;
    /**
     * The ActionTimestamp
     * Meta information extracted from the WSDL
     * - documentation: YYYY-MM-DDThh:mm:ss.sss
     * - base: string
     * - minOccurs: 0
     * - pattern: [12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?
     *
     * @var string|null
     */
    protected ?string $ActionTimestamp = null;
    /**
     * The Comments
     * Meta information extracted from the WSDL
     * - base: string
     * - maxLength: 250
     * - minOccurs: 0
     *
     * @var string|null
     */
    protected ?string $Comments = null;

    public function __construct(
        ?string $ethocaID = null,
        ?string $outcome = null,
        ?string $refunded = null,
        ?string $firstPartyFraud = null,
        ?float $amountStopped = null,
        ?string $actionTimestamp = null,
        ?string $comments = null
    ) {
        $this
            ->setEthocaID($ethocaID)
            ->setOutcome($outcome)
            ->setRefunded($refunded)
            ->setFirstPartyFraud($firstPartyFraud)
            ->setAmountStopped($amountStopped)
            ->setActionTimestamp($actionTimestamp)
            ->setComments($comments);
    }

    /**
     * Get EthocaID value
     *
     * @return string|null
     */
    public function getEthocaID(): ?string
    {
        return $this->EthocaID;
    }

    /**
     * Set EthocaID value
     *
     * @param string $ethocaID
     * @return AlertUpdateType
     */
    public function setEthocaID(?string $ethocaID = null): self
    {
        // validation for constraint: string
        if (!is_null($ethocaID) && !is_string($ethocaID)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($ethocaID, true),
                gettype($ethocaID)
            ), __LINE__);
        }
        // validation for constraint: length(25)
        if (!is_null($ethocaID) && mb_strlen((string)$ethocaID) !== 25) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be equal to 25',
                mb_strlen((string)$ethocaID)
            ), __LINE__);
        }
        $this->EthocaID = $ethocaID;

        return $this;
    }

    /**
     * Get Outcome value
     *
     * @return string|null
     */
    public function getOutcome(): ?string
    {
        return $this->Outcome;
    }

    /**
     * Set Outcome value
     *
     * @param string $outcome
     * @return AlertUpdateType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Outcome::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Outcome::valueIsValid()
     */
    public function setOutcome(?string $outcome = null): self
    {
        // validation for constraint: enumeration
        if (!Outcome::valueIsValid($outcome)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Outcome',
                is_array($outcome) ? implode(', ', $outcome) : var_export($outcome, true),
                implode(', ', Outcome::getValidValues())
            ), __LINE__);
        }
        $this->Outcome = $outcome;

        return $this;
    }

    /**
     * Get Refunded value
     *
     * @return string|null
     */
    public function getRefunded(): ?string
    {
        return $this->Refunded;
    }

    /**
     * Set Refunded value
     *
     * @param string $refunded
     * @return AlertUpdateType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Refunded::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\Refunded::valueIsValid()
     */
    public function setRefunded(?string $refunded = null): self
    {
        // validation for constraint: enumeration
        if (!Refunded::valueIsValid($refunded)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\Refunded',
                is_array($refunded) ? implode(', ', $refunded) : var_export($refunded, true),
                implode(', ', Refunded::getValidValues())
            ), __LINE__);
        }
        $this->Refunded = $refunded;

        return $this;
    }

    /**
     * Get FirstPartyFraud value
     *
     * @return string|null
     */
    public function getFirstPartyFraud(): ?string
    {
        return $this->FirstPartyFraud;
    }

    /**
     * Set FirstPartyFraud value
     *
     * @param string $firstPartyFraud
     * @return AlertUpdateType
     * @throws InvalidArgumentException
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\FirstPartyFraud::getValidValues()
     * @uses \Ratespecial\Ethoca\Alerts\EnumType\FirstPartyFraud::valueIsValid()
     */
    public function setFirstPartyFraud(?string $firstPartyFraud = null): self
    {
        // validation for constraint: enumeration
        if (!FirstPartyFraud::valueIsValid($firstPartyFraud)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value(s) %s, please use one of: %s from enumeration class \Ratespecial\Ethoca\Alerts\EnumType\FirstPartyFraud',
                is_array($firstPartyFraud) ? implode(', ', $firstPartyFraud) : var_export($firstPartyFraud, true),
                implode(', ', FirstPartyFraud::getValidValues())
            ), __LINE__);
        }
        $this->FirstPartyFraud = $firstPartyFraud;

        return $this;
    }

    /**
     * Get AmountStopped value
     *
     * @return float|null
     */
    public function getAmountStopped(): ?float
    {
        return $this->AmountStopped;
    }

    /**
     * Set AmountStopped value
     *
     * @param float $amountStopped
     * @return AlertUpdateType
     */
    public function setAmountStopped(?float $amountStopped = null): self
    {
        // validation for constraint: float
        if (!is_null($amountStopped) && !(is_float($amountStopped) || is_numeric($amountStopped))) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a float value, %s given',
                var_export($amountStopped, true),
                gettype($amountStopped)
            ), __LINE__);
        }
        // validation for constraint: fractionDigits(3)
        if (
            !is_null($amountStopped) && mb_strlen(mb_substr(
                (string)$amountStopped,
                false !== mb_strpos((string)$amountStopped, '.') ? mb_strpos(
                    (string)$amountStopped,
                    '.'
                ) + 1 : mb_strlen((string)$amountStopped)
            )) > 3
        ) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid value %s, the value must at most contain 3 fraction digits, %d given',
                    var_export($amountStopped, true),
                    mb_strlen(mb_substr((string)$amountStopped, mb_strpos((string)$amountStopped, '.') + 1))
                ),
                __LINE__
            );
        }
        // validation for constraint: minInclusive
        if (!is_null($amountStopped) && $amountStopped < 0) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must be numerically greater than or equal to 0',
                var_export($amountStopped, true)
            ), __LINE__);
        }
        // validation for constraint: totalDigits(11)
        if (!is_null($amountStopped) && mb_strlen(preg_replace('/(\D)/', '', (string)$amountStopped)) > 11) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, the value must use at most 11 digits, "%d" given',
                var_export($amountStopped, true),
                mb_strlen(preg_replace('/(\D)/', '', (string)$amountStopped))
            ), __LINE__);
        }
        $this->AmountStopped = $amountStopped;

        return $this;
    }

    /**
     * Get ActionTimestamp value
     *
     * @return string|null
     */
    public function getActionTimestamp(): ?string
    {
        return $this->ActionTimestamp;
    }

    /**
     * Set ActionTimestamp value
     *
     * @param string $actionTimestamp
     * @return AlertUpdateType
     */
    public function setActionTimestamp(?string $actionTimestamp = null): self
    {
        // validation for constraint: string
        if (!is_null($actionTimestamp) && !is_string($actionTimestamp)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($actionTimestamp, true),
                gettype($actionTimestamp)
            ), __LINE__);
        }
        // validation for constraint: pattern([12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\.[0-9]{1,3})?)
        if (
            !is_null($actionTimestamp) && !preg_match(
                '/[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                (string)$actionTimestamp
            )
        ) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a literal that is among the set of character sequences denoted by the regular expression /[12]{1}[0-9]{3}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]{1,3})?/',
                var_export($actionTimestamp, true)
            ), __LINE__);
        }
        $this->ActionTimestamp = $actionTimestamp;

        return $this;
    }

    /**
     * Get Comments value
     *
     * @return string|null
     */
    public function getComments(): ?string
    {
        return $this->Comments;
    }

    /**
     * Set Comments value
     *
     * @param string $comments
     * @return AlertUpdateType
     */
    public function setComments(?string $comments = null): self
    {
        // validation for constraint: string
        if (!is_null($comments) && !is_string($comments)) {
            throw new InvalidArgumentException(sprintf(
                'Invalid value %s, please provide a string, %s given',
                var_export($comments, true),
                gettype($comments)
            ), __LINE__);
        }
        // validation for constraint: maxLength(250)
        if (!is_null($comments) && mb_strlen((string)$comments) > 250) {
            throw new InvalidArgumentException(sprintf(
                'Invalid length of %s, the number of characters/octets contained by the literal must be less than or equal to 250',
                mb_strlen((string)$comments)
            ), __LINE__);
        }
        $this->Comments = $comments;

        return $this;
    }
}
