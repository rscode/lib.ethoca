<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * Collection of updates.
 */
#[\AllowDynamicProperties]
class AlertUpdatesType extends AbstractStructBase
{
    /**
     * The AlertUpdate
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     *
     * @var AlertUpdateType[]
     */
    protected array $AlertUpdate = [];

    public function __construct(array $alertUpdate = [])
    {
        $this
            ->setAlertUpdate($alertUpdate);
    }

    /**
     * This method is responsible for validating the value(s) passed to the setAlertUpdate method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlertUpdate method
     * This has to validate that each item contained by the array match the itemType constraint
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAlertUpdateForArrayConstraintFromSetAlertUpdate(array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $alertUpdatesTypeAlertUpdateItem) {
            // validation for constraint: itemType
            if (!$alertUpdatesTypeAlertUpdateItem instanceof AlertUpdateType) {
                $invalidValues[] = is_object($alertUpdatesTypeAlertUpdateItem) ? get_class($alertUpdatesTypeAlertUpdateItem) : sprintf(
                    '%s(%s)',
                    gettype($alertUpdatesTypeAlertUpdateItem),
                    var_export($alertUpdatesTypeAlertUpdateItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The AlertUpdate property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\AlertUpdateType, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get AlertUpdate value
     *
     * @return AlertUpdateType[]
     */
    public function getAlertUpdate(): array
    {
        return $this->AlertUpdate;
    }

    /**
     * Set AlertUpdate value
     *
     * @param AlertUpdateType[] $alertUpdate
     * @return AlertUpdatesType
     * @throws InvalidArgumentException
     */
    public function setAlertUpdate(array $alertUpdate = []): self
    {
        // validation for constraint: array
        if ('' !== ($alertUpdateArrayErrorMessage = self::validateAlertUpdateForArrayConstraintFromSetAlertUpdate($alertUpdate))) {
            throw new InvalidArgumentException($alertUpdateArrayErrorMessage, __LINE__);
        }
        $this->AlertUpdate = $alertUpdate;

        return $this;
    }

    /**
     * Add item to AlertUpdate value
     *
     * @param AlertUpdateType $item
     * @return AlertUpdatesType
     * @throws InvalidArgumentException
     */
    public function addToAlertUpdate(AlertUpdateType $item): self
    {
        $this->AlertUpdate[] = $item;

        return $this;
    }
}
