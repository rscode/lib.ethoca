<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\StructType;

use InvalidArgumentException;
use WsdlToPhp\PackageBase\AbstractStructBase;

/**
 * This class stands for AlertAcknowledgementRequestType StructType
 *
 * @subpackage Structs
 */
#[\AllowDynamicProperties]
class AlertAcknowledgementRequestType extends AbstractStructBase
{
    /**
     * The AlertAcknowledgement
     * Meta information extracted from the WSDL
     * - maxOccurs: unbounded
     *
     * @var EthocaAcknowledgementRequestType[]
     */
    protected ?array $AlertAcknowledgement = null;

    /**
     * Constructor method for AlertAcknowledgementRequestType
     *
     * @param EthocaAcknowledgementRequestType[] $alertAcknowledgement
     * @uses AlertAcknowledgementRequestType::setAlertAcknowledgement()
     */
    public function __construct(?array $alertAcknowledgement = null)
    {
        $this
            ->setAlertAcknowledgement($alertAcknowledgement);
    }

    /**
     * This method is responsible for validating the value(s) passed to the setAlertAcknowledgement method
     * This method is willingly generated in order to preserve the one-line inline validation within the setAlertAcknowledgement method
     * This has to validate that each item contained by the array match the itemType constraint
     *
     * @param array $values
     * @return string A non-empty message if the values does not match the validation rules
     */
    public static function validateAlertAcknowledgementForArrayConstraintFromSetAlertAcknowledgement(?array $values = []): string
    {
        if (!is_array($values)) {
            return '';
        }
        $message       = '';
        $invalidValues = [];
        foreach ($values as $alertAcknowledgementRequestTypeAlertAcknowledgementItem) {
            // validation for constraint: itemType
            if (!$alertAcknowledgementRequestTypeAlertAcknowledgementItem instanceof EthocaAcknowledgementRequestType) {
                $invalidValues[] = is_object($alertAcknowledgementRequestTypeAlertAcknowledgementItem) ? get_class($alertAcknowledgementRequestTypeAlertAcknowledgementItem) : sprintf(
                    '%s(%s)',
                    gettype($alertAcknowledgementRequestTypeAlertAcknowledgementItem),
                    var_export($alertAcknowledgementRequestTypeAlertAcknowledgementItem, true)
                );
            }
        }
        if (!empty($invalidValues)) {
            $message = sprintf(
                'The AlertAcknowledgement property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\EthocaAcknowledgementRequestType, %s given',
                is_object($invalidValues) ? get_class($invalidValues) : (is_array($invalidValues) ? implode(
                    ', ',
                    $invalidValues
                ) : gettype($invalidValues))
            );
        }
        unset($invalidValues);

        return $message;
    }

    /**
     * Get AlertAcknowledgement value
     *
     * @return EthocaAcknowledgementRequestType[]
     */
    public function getAlertAcknowledgement(): ?array
    {
        return $this->AlertAcknowledgement;
    }

    /**
     * Set AlertAcknowledgement value
     *
     * @param EthocaAcknowledgementRequestType[] $alertAcknowledgement
     * @return AlertAcknowledgementRequestType
     * @throws InvalidArgumentException
     */
    public function setAlertAcknowledgement(?array $alertAcknowledgement = null): self
    {
        // validation for constraint: array
        if ('' !== ($alertAcknowledgementArrayErrorMessage = self::validateAlertAcknowledgementForArrayConstraintFromSetAlertAcknowledgement($alertAcknowledgement))) {
            throw new InvalidArgumentException($alertAcknowledgementArrayErrorMessage, __LINE__);
        }
        $this->AlertAcknowledgement = $alertAcknowledgement;

        return $this;
    }

    /**
     * Add item to AlertAcknowledgement value
     *
     * @param EthocaAcknowledgementRequestType $item
     * @return AlertAcknowledgementRequestType
     * @throws InvalidArgumentException
     */
    public function addToAlertAcknowledgement(EthocaAcknowledgementRequestType $item): self
    {
        // validation for constraint: itemType
        if (!$item instanceof EthocaAcknowledgementRequestType) {
            throw new InvalidArgumentException(sprintf(
                'The AlertAcknowledgement property can only contain items of type \Ratespecial\Ethoca\Alerts\StructType\EthocaAcknowledgementRequestType, %s given',
                is_object($item) ? get_class($item) : (is_array($item) ? implode(', ', $item) : gettype($item))
            ), __LINE__);
        }
        $this->AlertAcknowledgement[] = $item;

        return $this;
    }
}
