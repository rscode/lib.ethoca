<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for InitiatedBy EnumType
 *
 * @subpackage Enumerations
 */
class InitiatedBy extends AbstractStructEnumBase
{
    /**
     * Constant for value 'issuer'
     *
     * @return string 'issuer'
     */
    public const VALUE_ISSUER = 'issuer';
    /**
     * Constant for value 'cardholder'
     *
     * @return string 'cardholder'
     */
    public const VALUE_CARDHOLDER = 'cardholder';
    /**
     * Constant for value 'not_available'
     *
     * @return string 'not_available'
     */
    public const VALUE_NOT_AVAILABLE = 'not_available';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_CARDHOLDER
     * @uses self::VALUE_NOT_AVAILABLE
     * @uses self::VALUE_ISSUER
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ISSUER,
            self::VALUE_CARDHOLDER,
            self::VALUE_NOT_AVAILABLE,
        ];
    }
}
