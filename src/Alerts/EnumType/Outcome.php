<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Outcome EnumType
 *
 * @subpackage Enumerations
 */
class Outcome extends AbstractStructEnumBase
{
    /**
     * Constant for value 'stopped'
     *
     * @return string 'stopped'
     */
    public const VALUE_STOPPED = 'stopped';
    /**
     * Constant for value 'partially_stopped'
     *
     * @return string 'partially_stopped'
     */
    public const VALUE_PARTIALLY_STOPPED = 'partially_stopped';
    /**
     * Constant for value 'previously_cancelled'
     *
     * @return string 'previously_cancelled'
     */
    public const VALUE_PREVIOUSLY_CANCELLED = 'previously_cancelled';
    /**
     * Constant for value 'missed'
     *
     * @return string 'missed'
     */
    public const VALUE_MISSED = 'missed';
    /**
     * Constant for value 'notfound'
     *
     * @return string 'notfound'
     */
    public const VALUE_NOTFOUND = 'notfound';
    /**
     * Constant for value 'other'
     *
     * @return string 'other'
     */
    public const VALUE_OTHER = 'other';
    /**
     * Constant for value 'shipper_contacted'
     *
     * @return string 'shipper_contacted'
     */
    public const VALUE_SHIPPER_CONTACTED = 'shipper_contacted';
    /**
     * Constant for value 'account_suspended'
     *
     * @return string 'account_suspended'
     */
    public const VALUE_ACCOUNT_SUSPENDED = 'account_suspended';
    /**
     * Constant for value 'in_progress'
     *
     * @return string 'in_progress'
     */
    public const VALUE_IN_PROGRESS = 'in_progress';
    /**
     * Constant for value 'resolved'
     *
     * @return string 'resolved'
     */
    public const VALUE_RESOLVED = 'resolved';
    /**
     * Constant for value 'previously_refunded'
     *
     * @return string 'previously_refunded'
     */
    public const VALUE_PREVIOUSLY_REFUNDED = 'previously_refunded';
    /**
     * Constant for value 'unresolved_dispute'
     *
     * @return string 'unresolved_dispute'
     */
    public const VALUE_UNRESOLVED_DISPUTE = 'unresolved_dispute';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_STOPPED
     * @uses self::VALUE_PARTIALLY_STOPPED
     * @uses self::VALUE_PREVIOUSLY_CANCELLED
     * @uses self::VALUE_MISSED
     * @uses self::VALUE_NOTFOUND
     * @uses self::VALUE_OTHER
     * @uses self::VALUE_SHIPPER_CONTACTED
     * @uses self::VALUE_ACCOUNT_SUSPENDED
     * @uses self::VALUE_IN_PROGRESS
     * @uses self::VALUE_RESOLVED
     * @uses self::VALUE_PREVIOUSLY_REFUNDED
     * @uses self::VALUE_UNRESOLVED_DISPUTE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_STOPPED,
            self::VALUE_PARTIALLY_STOPPED,
            self::VALUE_PREVIOUSLY_CANCELLED,
            self::VALUE_MISSED,
            self::VALUE_NOTFOUND,
            self::VALUE_OTHER,
            self::VALUE_SHIPPER_CONTACTED,
            self::VALUE_ACCOUNT_SUSPENDED,
            self::VALUE_IN_PROGRESS,
            self::VALUE_RESOLVED,
            self::VALUE_PREVIOUSLY_REFUNDED,
            self::VALUE_UNRESOLVED_DISPUTE,
        ];
    }
}
