<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Status EnumType
 *
 * @subpackage Enumerations
 */
class Status extends AbstractStructEnumBase
{
    /**
     * Constant for value 'continue'
     *
     * @return string 'continue'
     */
    public const VALUE_CONTINUE = 'continue';
    /**
     * Constant for value 'success'
     *
     * @return string 'success'
     */
    public const VALUE_SUCCESS = 'success';
    /**
     * Constant for value 'failed'
     *
     * @return string 'failed'
     */
    public const VALUE_FAILED = 'failed';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_SUCCESS
     * @uses self::VALUE_FAILED
     * @uses self::VALUE_CONTINUE
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_CONTINUE,
            self::VALUE_SUCCESS,
            self::VALUE_FAILED,
        ];
    }
}
