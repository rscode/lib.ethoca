<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Is3DSecure EnumType
 *
 * @subpackage Enumerations
 */
class Is3DSecure extends AbstractStructEnumBase
{
    /**
     * Constant for value 'yes'
     *
     * @return string 'yes'
     */
    public const VALUE_YES = 'yes';
    /**
     * Constant for value 'no'
     *
     * @return string 'no'
     */
    public const VALUE_NO = 'no';
    /**
     * Constant for value 'not_available'
     *
     * @return string 'not_available'
     */
    public const VALUE_NOT_AVAILABLE = 'not_available';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NO
     * @uses self::VALUE_NOT_AVAILABLE
     * @uses self::VALUE_YES
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_YES,
            self::VALUE_NO,
            self::VALUE_NOT_AVAILABLE,
        ];
    }
}
