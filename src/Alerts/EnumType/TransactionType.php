<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for TransactionType EnumType
 *
 * @subpackage Enumerations
 */
class TransactionType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'keyed'
     *
     * @return string 'keyed'
     */
    public const VALUE_KEYED = 'keyed';
    /**
     * Constant for value 'swiped'
     *
     * @return string 'swiped'
     */
    public const VALUE_SWIPED = 'swiped';
    /**
     * Constant for value 'eCommerce'
     *
     * @return string 'eCommerce'
     */
    public const VALUE_E_COMMERCE = 'eCommerce';
    /**
     * Constant for value 'unknown'
     *
     * @return string 'unknown'
     */
    public const VALUE_UNKNOWN = 'unknown';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_SWIPED
     * @uses self::VALUE_E_COMMERCE
     * @uses self::VALUE_UNKNOWN
     * @uses self::VALUE_KEYED
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_KEYED,
            self::VALUE_SWIPED,
            self::VALUE_E_COMMERCE,
            self::VALUE_UNKNOWN,
        ];
    }
}
