<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for Refunded EnumType
 *
 * @subpackage Enumerations
 */
class Refunded extends AbstractStructEnumBase
{
    /**
     * Constant for value 'yes'
     *
     * @return string 'yes'
     */
    public const VALUE_YES = 'yes';
    /**
     * Constant for value 'no'
     *
     * @return string 'no'
     */
    public const VALUE_NO = 'no';
    /**
     * Constant for value 'refunded'
     *
     * @return string 'refunded'
     */
    public const VALUE_REFUNDED = 'refunded';
    /**
     * Constant for value 'not refunded'
     *
     * @return string 'not refunded'
     */
    public const VALUE_NOT_REFUNDED = 'not refunded';
    /**
     * Constant for value 'not settled'
     *
     * @return string 'not settled'
     */
    public const VALUE_NOT_SETTLED = 'not settled';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NO
     * @uses self::VALUE_REFUNDED
     * @uses self::VALUE_NOT_REFUNDED
     * @uses self::VALUE_NOT_SETTLED
     * @uses self::VALUE_YES
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_YES,
            self::VALUE_NO,
            self::VALUE_REFUNDED,
            self::VALUE_NOT_REFUNDED,
            self::VALUE_NOT_SETTLED,
        ];
    }
}
