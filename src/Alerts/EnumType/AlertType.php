<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for AlertType EnumType
 *
 * @subpackage Enumerations
 */
class AlertType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'issuer_alert'
     *
     * @return string 'issuer_alert'
     */
    public const VALUE_ISSUER_ALERT = 'issuer_alert';
    /**
     * Constant for value 'fraudreporter_alert'
     *
     * @return string 'fraudreporter_alert'
     */
    public const VALUE_FRAUDREPORTER_ALERT = 'fraudreporter_alert';
    /**
     * Constant for value 'customerdispute_alert'
     *
     * @return string 'customerdispute_alert'
     */
    public const VALUE_CUSTOMERDISPUTE_ALERT = 'customerdispute_alert';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_FRAUDREPORTER_ALERT
     * @uses self::VALUE_CUSTOMERDISPUTE_ALERT
     * @uses self::VALUE_ISSUER_ALERT
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_ISSUER_ALERT,
            self::VALUE_FRAUDREPORTER_ALERT,
            self::VALUE_CUSTOMERDISPUTE_ALERT,
        ];
    }
}
