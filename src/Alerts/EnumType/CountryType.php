<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for CountryType EnumType
 *
 * @subpackage Enumerations
 */
class CountryType extends AbstractStructEnumBase
{
    /**
     * Constant for value 'AF'
     *
     * @return string 'AF'
     */
    public const VALUE_AF = 'AF';
    /**
     * Constant for value 'AX'
     *
     * @return string 'AX'
     */
    public const VALUE_AX = 'AX';
    /**
     * Constant for value 'AL'
     *
     * @return string 'AL'
     */
    public const VALUE_AL = 'AL';
    /**
     * Constant for value 'DZ'
     *
     * @return string 'DZ'
     */
    public const VALUE_DZ = 'DZ';
    /**
     * Constant for value 'AS'
     *
     * @return string 'AS'
     */
    public const VALUE_AS = 'AS';
    /**
     * Constant for value 'AD'
     *
     * @return string 'AD'
     */
    public const VALUE_AD = 'AD';
    /**
     * Constant for value 'AO'
     *
     * @return string 'AO'
     */
    public const VALUE_AO = 'AO';
    /**
     * Constant for value 'AI'
     *
     * @return string 'AI'
     */
    public const VALUE_AI = 'AI';
    /**
     * Constant for value 'AQ'
     *
     * @return string 'AQ'
     */
    public const VALUE_AQ = 'AQ';
    /**
     * Constant for value 'AG'
     *
     * @return string 'AG'
     */
    public const VALUE_AG = 'AG';
    /**
     * Constant for value 'AR'
     *
     * @return string 'AR'
     */
    public const VALUE_AR = 'AR';
    /**
     * Constant for value 'AM'
     *
     * @return string 'AM'
     */
    public const VALUE_AM = 'AM';
    /**
     * Constant for value 'AW'
     *
     * @return string 'AW'
     */
    public const VALUE_AW = 'AW';
    /**
     * Constant for value 'AU'
     *
     * @return string 'AU'
     */
    public const VALUE_AU = 'AU';
    /**
     * Constant for value 'AT'
     *
     * @return string 'AT'
     */
    public const VALUE_AT = 'AT';
    /**
     * Constant for value 'AZ'
     *
     * @return string 'AZ'
     */
    public const VALUE_AZ = 'AZ';
    /**
     * Constant for value 'BS'
     *
     * @return string 'BS'
     */
    public const VALUE_BS = 'BS';
    /**
     * Constant for value 'BH'
     *
     * @return string 'BH'
     */
    public const VALUE_BH = 'BH';
    /**
     * Constant for value 'BD'
     *
     * @return string 'BD'
     */
    public const VALUE_BD = 'BD';
    /**
     * Constant for value 'BB'
     *
     * @return string 'BB'
     */
    public const VALUE_BB = 'BB';
    /**
     * Constant for value 'BY'
     *
     * @return string 'BY'
     */
    public const VALUE_BY = 'BY';
    /**
     * Constant for value 'BE'
     *
     * @return string 'BE'
     */
    public const VALUE_BE = 'BE';
    /**
     * Constant for value 'BZ'
     *
     * @return string 'BZ'
     */
    public const VALUE_BZ = 'BZ';
    /**
     * Constant for value 'BJ'
     *
     * @return string 'BJ'
     */
    public const VALUE_BJ = 'BJ';
    /**
     * Constant for value 'BM'
     *
     * @return string 'BM'
     */
    public const VALUE_BM = 'BM';
    /**
     * Constant for value 'BT'
     *
     * @return string 'BT'
     */
    public const VALUE_BT = 'BT';
    /**
     * Constant for value 'BO'
     *
     * @return string 'BO'
     */
    public const VALUE_BO = 'BO';
    /**
     * Constant for value 'BA'
     *
     * @return string 'BA'
     */
    public const VALUE_BA = 'BA';
    /**
     * Constant for value 'BW'
     *
     * @return string 'BW'
     */
    public const VALUE_BW = 'BW';
    /**
     * Constant for value 'BV'
     *
     * @return string 'BV'
     */
    public const VALUE_BV = 'BV';
    /**
     * Constant for value 'BR'
     *
     * @return string 'BR'
     */
    public const VALUE_BR = 'BR';
    /**
     * Constant for value 'IO'
     *
     * @return string 'IO'
     */
    public const VALUE_IO = 'IO';
    /**
     * Constant for value 'BN'
     *
     * @return string 'BN'
     */
    public const VALUE_BN = 'BN';
    /**
     * Constant for value 'BG'
     *
     * @return string 'BG'
     */
    public const VALUE_BG = 'BG';
    /**
     * Constant for value 'BF'
     *
     * @return string 'BF'
     */
    public const VALUE_BF = 'BF';
    /**
     * Constant for value 'BI'
     *
     * @return string 'BI'
     */
    public const VALUE_BI = 'BI';
    /**
     * Constant for value 'KH'
     *
     * @return string 'KH'
     */
    public const VALUE_KH = 'KH';
    /**
     * Constant for value 'CM'
     *
     * @return string 'CM'
     */
    public const VALUE_CM = 'CM';
    /**
     * Constant for value 'CA'
     *
     * @return string 'CA'
     */
    public const VALUE_CA = 'CA';
    /**
     * Constant for value 'CV'
     *
     * @return string 'CV'
     */
    public const VALUE_CV = 'CV';
    /**
     * Constant for value 'KY'
     *
     * @return string 'KY'
     */
    public const VALUE_KY = 'KY';
    /**
     * Constant for value 'CF'
     *
     * @return string 'CF'
     */
    public const VALUE_CF = 'CF';
    /**
     * Constant for value 'TD'
     *
     * @return string 'TD'
     */
    public const VALUE_TD = 'TD';
    /**
     * Constant for value 'CL'
     *
     * @return string 'CL'
     */
    public const VALUE_CL = 'CL';
    /**
     * Constant for value 'CN'
     *
     * @return string 'CN'
     */
    public const VALUE_CN = 'CN';
    /**
     * Constant for value 'CX'
     *
     * @return string 'CX'
     */
    public const VALUE_CX = 'CX';
    /**
     * Constant for value 'CC'
     *
     * @return string 'CC'
     */
    public const VALUE_CC = 'CC';
    /**
     * Constant for value 'CO'
     *
     * @return string 'CO'
     */
    public const VALUE_CO = 'CO';
    /**
     * Constant for value 'KM'
     *
     * @return string 'KM'
     */
    public const VALUE_KM = 'KM';
    /**
     * Constant for value 'CG'
     *
     * @return string 'CG'
     */
    public const VALUE_CG = 'CG';
    /**
     * Constant for value 'CD'
     *
     * @return string 'CD'
     */
    public const VALUE_CD = 'CD';
    /**
     * Constant for value 'CK'
     *
     * @return string 'CK'
     */
    public const VALUE_CK = 'CK';
    /**
     * Constant for value 'CR'
     *
     * @return string 'CR'
     */
    public const VALUE_CR = 'CR';
    /**
     * Constant for value 'CI'
     *
     * @return string 'CI'
     */
    public const VALUE_CI = 'CI';
    /**
     * Constant for value 'HR'
     *
     * @return string 'HR'
     */
    public const VALUE_HR = 'HR';
    /**
     * Constant for value 'CU'
     *
     * @return string 'CU'
     */
    public const VALUE_CU = 'CU';
    /**
     * Constant for value 'CY'
     *
     * @return string 'CY'
     */
    public const VALUE_CY = 'CY';
    /**
     * Constant for value 'CZ'
     *
     * @return string 'CZ'
     */
    public const VALUE_CZ = 'CZ';
    /**
     * Constant for value 'DK'
     *
     * @return string 'DK'
     */
    public const VALUE_DK = 'DK';
    /**
     * Constant for value 'DJ'
     *
     * @return string 'DJ'
     */
    public const VALUE_DJ = 'DJ';
    /**
     * Constant for value 'DM'
     *
     * @return string 'DM'
     */
    public const VALUE_DM = 'DM';
    /**
     * Constant for value 'DO'
     *
     * @return string 'DO'
     */
    public const VALUE_DO = 'DO';
    /**
     * Constant for value 'EC'
     *
     * @return string 'EC'
     */
    public const VALUE_EC = 'EC';
    /**
     * Constant for value 'EG'
     *
     * @return string 'EG'
     */
    public const VALUE_EG = 'EG';
    /**
     * Constant for value 'SV'
     *
     * @return string 'SV'
     */
    public const VALUE_SV = 'SV';
    /**
     * Constant for value 'GQ'
     *
     * @return string 'GQ'
     */
    public const VALUE_GQ = 'GQ';
    /**
     * Constant for value 'ER'
     *
     * @return string 'ER'
     */
    public const VALUE_ER = 'ER';
    /**
     * Constant for value 'EE'
     *
     * @return string 'EE'
     */
    public const VALUE_EE = 'EE';
    /**
     * Constant for value 'ET'
     *
     * @return string 'ET'
     */
    public const VALUE_ET = 'ET';
    /**
     * Constant for value 'FK'
     *
     * @return string 'FK'
     */
    public const VALUE_FK = 'FK';
    /**
     * Constant for value 'FO'
     *
     * @return string 'FO'
     */
    public const VALUE_FO = 'FO';
    /**
     * Constant for value 'FJ'
     *
     * @return string 'FJ'
     */
    public const VALUE_FJ = 'FJ';
    /**
     * Constant for value 'FI'
     *
     * @return string 'FI'
     */
    public const VALUE_FI = 'FI';
    /**
     * Constant for value 'FR'
     *
     * @return string 'FR'
     */
    public const VALUE_FR = 'FR';
    /**
     * Constant for value 'FX'
     *
     * @return string 'FX'
     */
    public const VALUE_FX = 'FX';
    /**
     * Constant for value 'GF'
     *
     * @return string 'GF'
     */
    public const VALUE_GF = 'GF';
    /**
     * Constant for value 'PF'
     *
     * @return string 'PF'
     */
    public const VALUE_PF = 'PF';
    /**
     * Constant for value 'TF'
     *
     * @return string 'TF'
     */
    public const VALUE_TF = 'TF';
    /**
     * Constant for value 'GA'
     *
     * @return string 'GA'
     */
    public const VALUE_GA = 'GA';
    /**
     * Constant for value 'GM'
     *
     * @return string 'GM'
     */
    public const VALUE_GM = 'GM';
    /**
     * Constant for value 'GE'
     *
     * @return string 'GE'
     */
    public const VALUE_GE = 'GE';
    /**
     * Constant for value 'DE'
     *
     * @return string 'DE'
     */
    public const VALUE_DE = 'DE';
    /**
     * Constant for value 'GH'
     *
     * @return string 'GH'
     */
    public const VALUE_GH = 'GH';
    /**
     * Constant for value 'GI'
     *
     * @return string 'GI'
     */
    public const VALUE_GI = 'GI';
    /**
     * Constant for value 'GR'
     *
     * @return string 'GR'
     */
    public const VALUE_GR = 'GR';
    /**
     * Constant for value 'GL'
     *
     * @return string 'GL'
     */
    public const VALUE_GL = 'GL';
    /**
     * Constant for value 'GD'
     *
     * @return string 'GD'
     */
    public const VALUE_GD = 'GD';
    /**
     * Constant for value 'GP'
     *
     * @return string 'GP'
     */
    public const VALUE_GP = 'GP';
    /**
     * Constant for value 'GU'
     *
     * @return string 'GU'
     */
    public const VALUE_GU = 'GU';
    /**
     * Constant for value 'GT'
     *
     * @return string 'GT'
     */
    public const VALUE_GT = 'GT';
    /**
     * Constant for value 'GN'
     *
     * @return string 'GN'
     */
    public const VALUE_GN = 'GN';
    /**
     * Constant for value 'GW'
     *
     * @return string 'GW'
     */
    public const VALUE_GW = 'GW';
    /**
     * Constant for value 'GY'
     *
     * @return string 'GY'
     */
    public const VALUE_GY = 'GY';
    /**
     * Constant for value 'HT'
     *
     * @return string 'HT'
     */
    public const VALUE_HT = 'HT';
    /**
     * Constant for value 'HM'
     *
     * @return string 'HM'
     */
    public const VALUE_HM = 'HM';
    /**
     * Constant for value 'HN'
     *
     * @return string 'HN'
     */
    public const VALUE_HN = 'HN';
    /**
     * Constant for value 'HK'
     *
     * @return string 'HK'
     */
    public const VALUE_HK = 'HK';
    /**
     * Constant for value 'HU'
     *
     * @return string 'HU'
     */
    public const VALUE_HU = 'HU';
    /**
     * Constant for value 'IS'
     *
     * @return string 'IS'
     */
    public const VALUE_IS = 'IS';
    /**
     * Constant for value 'IN'
     *
     * @return string 'IN'
     */
    public const VALUE_IN = 'IN';
    /**
     * Constant for value 'ID'
     *
     * @return string 'ID'
     */
    public const VALUE_ID = 'ID';
    /**
     * Constant for value 'IR'
     *
     * @return string 'IR'
     */
    public const VALUE_IR = 'IR';
    /**
     * Constant for value 'IQ'
     *
     * @return string 'IQ'
     */
    public const VALUE_IQ = 'IQ';
    /**
     * Constant for value 'IE'
     *
     * @return string 'IE'
     */
    public const VALUE_IE = 'IE';
    /**
     * Constant for value 'IL'
     *
     * @return string 'IL'
     */
    public const VALUE_IL = 'IL';
    /**
     * Constant for value 'IT'
     *
     * @return string 'IT'
     */
    public const VALUE_IT = 'IT';
    /**
     * Constant for value 'JM'
     *
     * @return string 'JM'
     */
    public const VALUE_JM = 'JM';
    /**
     * Constant for value 'JP'
     *
     * @return string 'JP'
     */
    public const VALUE_JP = 'JP';
    /**
     * Constant for value 'JO'
     *
     * @return string 'JO'
     */
    public const VALUE_JO = 'JO';
    /**
     * Constant for value 'KZ'
     *
     * @return string 'KZ'
     */
    public const VALUE_KZ = 'KZ';
    /**
     * Constant for value 'KE'
     *
     * @return string 'KE'
     */
    public const VALUE_KE = 'KE';
    /**
     * Constant for value 'KI'
     *
     * @return string 'KI'
     */
    public const VALUE_KI = 'KI';
    /**
     * Constant for value 'KP'
     *
     * @return string 'KP'
     */
    public const VALUE_KP = 'KP';
    /**
     * Constant for value 'KR'
     *
     * @return string 'KR'
     */
    public const VALUE_KR = 'KR';
    /**
     * Constant for value 'KW'
     *
     * @return string 'KW'
     */
    public const VALUE_KW = 'KW';
    /**
     * Constant for value 'KG'
     *
     * @return string 'KG'
     */
    public const VALUE_KG = 'KG';
    /**
     * Constant for value 'LA'
     *
     * @return string 'LA'
     */
    public const VALUE_LA = 'LA';
    /**
     * Constant for value 'LV'
     *
     * @return string 'LV'
     */
    public const VALUE_LV = 'LV';
    /**
     * Constant for value 'LB'
     *
     * @return string 'LB'
     */
    public const VALUE_LB = 'LB';
    /**
     * Constant for value 'LS'
     *
     * @return string 'LS'
     */
    public const VALUE_LS = 'LS';
    /**
     * Constant for value 'LR'
     *
     * @return string 'LR'
     */
    public const VALUE_LR = 'LR';
    /**
     * Constant for value 'LY'
     *
     * @return string 'LY'
     */
    public const VALUE_LY = 'LY';
    /**
     * Constant for value 'LI'
     *
     * @return string 'LI'
     */
    public const VALUE_LI = 'LI';
    /**
     * Constant for value 'LT'
     *
     * @return string 'LT'
     */
    public const VALUE_LT = 'LT';
    /**
     * Constant for value 'LU'
     *
     * @return string 'LU'
     */
    public const VALUE_LU = 'LU';
    /**
     * Constant for value 'MO'
     *
     * @return string 'MO'
     */
    public const VALUE_MO = 'MO';
    /**
     * Constant for value 'MK'
     *
     * @return string 'MK'
     */
    public const VALUE_MK = 'MK';
    /**
     * Constant for value 'MG'
     *
     * @return string 'MG'
     */
    public const VALUE_MG = 'MG';
    /**
     * Constant for value 'MW'
     *
     * @return string 'MW'
     */
    public const VALUE_MW = 'MW';
    /**
     * Constant for value 'MY'
     *
     * @return string 'MY'
     */
    public const VALUE_MY = 'MY';
    /**
     * Constant for value 'MV'
     *
     * @return string 'MV'
     */
    public const VALUE_MV = 'MV';
    /**
     * Constant for value 'ML'
     *
     * @return string 'ML'
     */
    public const VALUE_ML = 'ML';
    /**
     * Constant for value 'MT'
     *
     * @return string 'MT'
     */
    public const VALUE_MT = 'MT';
    /**
     * Constant for value 'MH'
     *
     * @return string 'MH'
     */
    public const VALUE_MH = 'MH';
    /**
     * Constant for value 'MQ'
     *
     * @return string 'MQ'
     */
    public const VALUE_MQ = 'MQ';
    /**
     * Constant for value 'MR'
     *
     * @return string 'MR'
     */
    public const VALUE_MR = 'MR';
    /**
     * Constant for value 'MU'
     *
     * @return string 'MU'
     */
    public const VALUE_MU = 'MU';
    /**
     * Constant for value 'YT'
     *
     * @return string 'YT'
     */
    public const VALUE_YT = 'YT';
    /**
     * Constant for value 'MX'
     *
     * @return string 'MX'
     */
    public const VALUE_MX = 'MX';
    /**
     * Constant for value 'FM'
     *
     * @return string 'FM'
     */
    public const VALUE_FM = 'FM';
    /**
     * Constant for value 'MD'
     *
     * @return string 'MD'
     */
    public const VALUE_MD = 'MD';
    /**
     * Constant for value 'MC'
     *
     * @return string 'MC'
     */
    public const VALUE_MC = 'MC';
    /**
     * Constant for value 'MN'
     *
     * @return string 'MN'
     */
    public const VALUE_MN = 'MN';
    /**
     * Constant for value 'MS'
     *
     * @return string 'MS'
     */
    public const VALUE_MS = 'MS';
    /**
     * Constant for value 'MA'
     *
     * @return string 'MA'
     */
    public const VALUE_MA = 'MA';
    /**
     * Constant for value 'MZ'
     *
     * @return string 'MZ'
     */
    public const VALUE_MZ = 'MZ';
    /**
     * Constant for value 'MM'
     *
     * @return string 'MM'
     */
    public const VALUE_MM = 'MM';
    /**
     * Constant for value 'NA'
     *
     * @return string 'NA'
     */
    public const VALUE_NA = 'NA';
    /**
     * Constant for value 'NR'
     *
     * @return string 'NR'
     */
    public const VALUE_NR = 'NR';
    /**
     * Constant for value 'NP'
     *
     * @return string 'NP'
     */
    public const VALUE_NP = 'NP';
    /**
     * Constant for value 'NL'
     *
     * @return string 'NL'
     */
    public const VALUE_NL = 'NL';
    /**
     * Constant for value 'AN'
     *
     * @return string 'AN'
     */
    public const VALUE_AN = 'AN';
    /**
     * Constant for value 'NC'
     *
     * @return string 'NC'
     */
    public const VALUE_NC = 'NC';
    /**
     * Constant for value 'NZ'
     *
     * @return string 'NZ'
     */
    public const VALUE_NZ = 'NZ';
    /**
     * Constant for value 'NI'
     *
     * @return string 'NI'
     */
    public const VALUE_NI = 'NI';
    /**
     * Constant for value 'NE'
     *
     * @return string 'NE'
     */
    public const VALUE_NE = 'NE';
    /**
     * Constant for value 'NG'
     *
     * @return string 'NG'
     */
    public const VALUE_NG = 'NG';
    /**
     * Constant for value 'NU'
     *
     * @return string 'NU'
     */
    public const VALUE_NU = 'NU';
    /**
     * Constant for value 'NF'
     *
     * @return string 'NF'
     */
    public const VALUE_NF = 'NF';
    /**
     * Constant for value 'MP'
     *
     * @return string 'MP'
     */
    public const VALUE_MP = 'MP';
    /**
     * Constant for value 'NO'
     *
     * @return string 'NO'
     */
    public const VALUE_NO = 'NO';
    /**
     * Constant for value 'OM'
     *
     * @return string 'OM'
     */
    public const VALUE_OM = 'OM';
    /**
     * Constant for value 'PK'
     *
     * @return string 'PK'
     */
    public const VALUE_PK = 'PK';
    /**
     * Constant for value 'PW'
     *
     * @return string 'PW'
     */
    public const VALUE_PW = 'PW';
    /**
     * Constant for value 'PS'
     *
     * @return string 'PS'
     */
    public const VALUE_PS = 'PS';
    /**
     * Constant for value 'PA'
     *
     * @return string 'PA'
     */
    public const VALUE_PA = 'PA';
    /**
     * Constant for value 'PG'
     *
     * @return string 'PG'
     */
    public const VALUE_PG = 'PG';
    /**
     * Constant for value 'PY'
     *
     * @return string 'PY'
     */
    public const VALUE_PY = 'PY';
    /**
     * Constant for value 'PE'
     *
     * @return string 'PE'
     */
    public const VALUE_PE = 'PE';
    /**
     * Constant for value 'PH'
     *
     * @return string 'PH'
     */
    public const VALUE_PH = 'PH';
    /**
     * Constant for value 'PN'
     *
     * @return string 'PN'
     */
    public const VALUE_PN = 'PN';
    /**
     * Constant for value 'PL'
     *
     * @return string 'PL'
     */
    public const VALUE_PL = 'PL';
    /**
     * Constant for value 'PT'
     *
     * @return string 'PT'
     */
    public const VALUE_PT = 'PT';
    /**
     * Constant for value 'PR'
     *
     * @return string 'PR'
     */
    public const VALUE_PR = 'PR';
    /**
     * Constant for value 'QA'
     *
     * @return string 'QA'
     */
    public const VALUE_QA = 'QA';
    /**
     * Constant for value 'RE'
     *
     * @return string 'RE'
     */
    public const VALUE_RE = 'RE';
    /**
     * Constant for value 'RO'
     *
     * @return string 'RO'
     */
    public const VALUE_RO = 'RO';
    /**
     * Constant for value 'RU'
     *
     * @return string 'RU'
     */
    public const VALUE_RU = 'RU';
    /**
     * Constant for value 'RW'
     *
     * @return string 'RW'
     */
    public const VALUE_RW = 'RW';
    /**
     * Constant for value 'SH'
     *
     * @return string 'SH'
     */
    public const VALUE_SH = 'SH';
    /**
     * Constant for value 'KN'
     *
     * @return string 'KN'
     */
    public const VALUE_KN = 'KN';
    /**
     * Constant for value 'LC'
     *
     * @return string 'LC'
     */
    public const VALUE_LC = 'LC';
    /**
     * Constant for value 'PM'
     *
     * @return string 'PM'
     */
    public const VALUE_PM = 'PM';
    /**
     * Constant for value 'VC'
     *
     * @return string 'VC'
     */
    public const VALUE_VC = 'VC';
    /**
     * Constant for value 'WS'
     *
     * @return string 'WS'
     */
    public const VALUE_WS = 'WS';
    /**
     * Constant for value 'SM'
     *
     * @return string 'SM'
     */
    public const VALUE_SM = 'SM';
    /**
     * Constant for value 'ST'
     *
     * @return string 'ST'
     */
    public const VALUE_ST = 'ST';
    /**
     * Constant for value 'SA'
     *
     * @return string 'SA'
     */
    public const VALUE_SA = 'SA';
    /**
     * Constant for value 'SN'
     *
     * @return string 'SN'
     */
    public const VALUE_SN = 'SN';
    /**
     * Constant for value 'CS'
     *
     * @return string 'CS'
     */
    public const VALUE_CS = 'CS';
    /**
     * Constant for value 'SC'
     *
     * @return string 'SC'
     */
    public const VALUE_SC = 'SC';
    /**
     * Constant for value 'SL'
     *
     * @return string 'SL'
     */
    public const VALUE_SL = 'SL';
    /**
     * Constant for value 'SG'
     *
     * @return string 'SG'
     */
    public const VALUE_SG = 'SG';
    /**
     * Constant for value 'SK'
     *
     * @return string 'SK'
     */
    public const VALUE_SK = 'SK';
    /**
     * Constant for value 'SI'
     *
     * @return string 'SI'
     */
    public const VALUE_SI = 'SI';
    /**
     * Constant for value 'SB'
     *
     * @return string 'SB'
     */
    public const VALUE_SB = 'SB';
    /**
     * Constant for value 'SO'
     *
     * @return string 'SO'
     */
    public const VALUE_SO = 'SO';
    /**
     * Constant for value 'ZA'
     *
     * @return string 'ZA'
     */
    public const VALUE_ZA = 'ZA';
    /**
     * Constant for value 'GS'
     *
     * @return string 'GS'
     */
    public const VALUE_GS = 'GS';
    /**
     * Constant for value 'ES'
     *
     * @return string 'ES'
     */
    public const VALUE_ES = 'ES';
    /**
     * Constant for value 'LK'
     *
     * @return string 'LK'
     */
    public const VALUE_LK = 'LK';
    /**
     * Constant for value 'SD'
     *
     * @return string 'SD'
     */
    public const VALUE_SD = 'SD';
    /**
     * Constant for value 'SR'
     *
     * @return string 'SR'
     */
    public const VALUE_SR = 'SR';
    /**
     * Constant for value 'SJ'
     *
     * @return string 'SJ'
     */
    public const VALUE_SJ = 'SJ';
    /**
     * Constant for value 'SZ'
     *
     * @return string 'SZ'
     */
    public const VALUE_SZ = 'SZ';
    /**
     * Constant for value 'SE'
     *
     * @return string 'SE'
     */
    public const VALUE_SE = 'SE';
    /**
     * Constant for value 'CH'
     *
     * @return string 'CH'
     */
    public const VALUE_CH = 'CH';
    /**
     * Constant for value 'SY'
     *
     * @return string 'SY'
     */
    public const VALUE_SY = 'SY';
    /**
     * Constant for value 'TW'
     *
     * @return string 'TW'
     */
    public const VALUE_TW = 'TW';
    /**
     * Constant for value 'TJ'
     *
     * @return string 'TJ'
     */
    public const VALUE_TJ = 'TJ';
    /**
     * Constant for value 'TZ'
     *
     * @return string 'TZ'
     */
    public const VALUE_TZ = 'TZ';
    /**
     * Constant for value 'TH'
     *
     * @return string 'TH'
     */
    public const VALUE_TH = 'TH';
    /**
     * Constant for value 'TL'
     *
     * @return string 'TL'
     */
    public const VALUE_TL = 'TL';
    /**
     * Constant for value 'TG'
     *
     * @return string 'TG'
     */
    public const VALUE_TG = 'TG';
    /**
     * Constant for value 'TK'
     *
     * @return string 'TK'
     */
    public const VALUE_TK = 'TK';
    /**
     * Constant for value 'TO'
     *
     * @return string 'TO'
     */
    public const VALUE_TO = 'TO';
    /**
     * Constant for value 'TT'
     *
     * @return string 'TT'
     */
    public const VALUE_TT = 'TT';
    /**
     * Constant for value 'TN'
     *
     * @return string 'TN'
     */
    public const VALUE_TN = 'TN';
    /**
     * Constant for value 'TR'
     *
     * @return string 'TR'
     */
    public const VALUE_TR = 'TR';
    /**
     * Constant for value 'TM'
     *
     * @return string 'TM'
     */
    public const VALUE_TM = 'TM';
    /**
     * Constant for value 'TC'
     *
     * @return string 'TC'
     */
    public const VALUE_TC = 'TC';
    /**
     * Constant for value 'TV'
     *
     * @return string 'TV'
     */
    public const VALUE_TV = 'TV';
    /**
     * Constant for value 'UG'
     *
     * @return string 'UG'
     */
    public const VALUE_UG = 'UG';
    /**
     * Constant for value 'UA'
     *
     * @return string 'UA'
     */
    public const VALUE_UA = 'UA';
    /**
     * Constant for value 'AE'
     *
     * @return string 'AE'
     */
    public const VALUE_AE = 'AE';
    /**
     * Constant for value 'GB'
     *
     * @return string 'GB'
     */
    public const VALUE_GB = 'GB';
    /**
     * Constant for value 'US'
     *
     * @return string 'US'
     */
    public const VALUE_US = 'US';
    /**
     * Constant for value 'UM'
     *
     * @return string 'UM'
     */
    public const VALUE_UM = 'UM';
    /**
     * Constant for value 'UY'
     *
     * @return string 'UY'
     */
    public const VALUE_UY = 'UY';
    /**
     * Constant for value 'UZ'
     *
     * @return string 'UZ'
     */
    public const VALUE_UZ = 'UZ';
    /**
     * Constant for value 'VU'
     *
     * @return string 'VU'
     */
    public const VALUE_VU = 'VU';
    /**
     * Constant for value 'VA'
     *
     * @return string 'VA'
     */
    public const VALUE_VA = 'VA';
    /**
     * Constant for value 'VE'
     *
     * @return string 'VE'
     */
    public const VALUE_VE = 'VE';
    /**
     * Constant for value 'VN'
     *
     * @return string 'VN'
     */
    public const VALUE_VN = 'VN';
    /**
     * Constant for value 'VG'
     *
     * @return string 'VG'
     */
    public const VALUE_VG = 'VG';
    /**
     * Constant for value 'VI'
     *
     * @return string 'VI'
     */
    public const VALUE_VI = 'VI';
    /**
     * Constant for value 'WF'
     *
     * @return string 'WF'
     */
    public const VALUE_WF = 'WF';
    /**
     * Constant for value 'EH'
     *
     * @return string 'EH'
     */
    public const VALUE_EH = 'EH';
    /**
     * Constant for value 'YE'
     *
     * @return string 'YE'
     */
    public const VALUE_YE = 'YE';
    /**
     * Constant for value 'ZM'
     *
     * @return string 'ZM'
     */
    public const VALUE_ZM = 'ZM';
    /**
     * Constant for value 'ZW'
     *
     * @return string 'ZW'
     */
    public const VALUE_ZW = 'ZW';
    /**
     * Constant for value 'RS'
     *
     * @return string 'RS'
     */
    public const VALUE_RS = 'RS';
    /**
     * Constant for value 'IM'
     *
     * @return string 'IM'
     */
    public const VALUE_IM = 'IM';
    /**
     * Constant for value 'JE'
     *
     * @return string 'JE'
     */
    public const VALUE_JE = 'JE';
    /**
     * Constant for value 'TP'
     *
     * @return string 'TP'
     */
    public const VALUE_TP = 'TP';
    /**
     * Constant for value 'ME'
     *
     * @return string 'ME'
     */
    public const VALUE_ME = 'ME';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_AF
     * @uses self::VALUE_AX
     * @uses self::VALUE_AL
     * @uses self::VALUE_DZ
     * @uses self::VALUE_AS
     * @uses self::VALUE_AD
     * @uses self::VALUE_AO
     * @uses self::VALUE_AI
     * @uses self::VALUE_AQ
     * @uses self::VALUE_AG
     * @uses self::VALUE_AR
     * @uses self::VALUE_AM
     * @uses self::VALUE_AW
     * @uses self::VALUE_AU
     * @uses self::VALUE_AT
     * @uses self::VALUE_AZ
     * @uses self::VALUE_BS
     * @uses self::VALUE_BH
     * @uses self::VALUE_BD
     * @uses self::VALUE_BB
     * @uses self::VALUE_BY
     * @uses self::VALUE_BE
     * @uses self::VALUE_BZ
     * @uses self::VALUE_BJ
     * @uses self::VALUE_BM
     * @uses self::VALUE_BT
     * @uses self::VALUE_BO
     * @uses self::VALUE_BA
     * @uses self::VALUE_BW
     * @uses self::VALUE_BV
     * @uses self::VALUE_BR
     * @uses self::VALUE_IO
     * @uses self::VALUE_BN
     * @uses self::VALUE_BG
     * @uses self::VALUE_BF
     * @uses self::VALUE_BI
     * @uses self::VALUE_KH
     * @uses self::VALUE_CM
     * @uses self::VALUE_CA
     * @uses self::VALUE_CV
     * @uses self::VALUE_KY
     * @uses self::VALUE_CF
     * @uses self::VALUE_TD
     * @uses self::VALUE_CL
     * @uses self::VALUE_CN
     * @uses self::VALUE_CX
     * @uses self::VALUE_CC
     * @uses self::VALUE_CO
     * @uses self::VALUE_KM
     * @uses self::VALUE_CG
     * @uses self::VALUE_CD
     * @uses self::VALUE_CK
     * @uses self::VALUE_CR
     * @uses self::VALUE_CI
     * @uses self::VALUE_HR
     * @uses self::VALUE_CU
     * @uses self::VALUE_CY
     * @uses self::VALUE_CZ
     * @uses self::VALUE_DK
     * @uses self::VALUE_DJ
     * @uses self::VALUE_DM
     * @uses self::VALUE_DO
     * @uses self::VALUE_EC
     * @uses self::VALUE_EG
     * @uses self::VALUE_SV
     * @uses self::VALUE_GQ
     * @uses self::VALUE_ER
     * @uses self::VALUE_EE
     * @uses self::VALUE_ET
     * @uses self::VALUE_FK
     * @uses self::VALUE_FO
     * @uses self::VALUE_FJ
     * @uses self::VALUE_FI
     * @uses self::VALUE_FR
     * @uses self::VALUE_FX
     * @uses self::VALUE_GF
     * @uses self::VALUE_PF
     * @uses self::VALUE_TF
     * @uses self::VALUE_GA
     * @uses self::VALUE_GM
     * @uses self::VALUE_GE
     * @uses self::VALUE_DE
     * @uses self::VALUE_GH
     * @uses self::VALUE_GI
     * @uses self::VALUE_GR
     * @uses self::VALUE_GL
     * @uses self::VALUE_GD
     * @uses self::VALUE_GP
     * @uses self::VALUE_GU
     * @uses self::VALUE_GT
     * @uses self::VALUE_GN
     * @uses self::VALUE_GW
     * @uses self::VALUE_GY
     * @uses self::VALUE_HT
     * @uses self::VALUE_HM
     * @uses self::VALUE_HN
     * @uses self::VALUE_HK
     * @uses self::VALUE_HU
     * @uses self::VALUE_IS
     * @uses self::VALUE_IN
     * @uses self::VALUE_ID
     * @uses self::VALUE_IR
     * @uses self::VALUE_IQ
     * @uses self::VALUE_IE
     * @uses self::VALUE_IL
     * @uses self::VALUE_IT
     * @uses self::VALUE_JM
     * @uses self::VALUE_JP
     * @uses self::VALUE_JO
     * @uses self::VALUE_KZ
     * @uses self::VALUE_KE
     * @uses self::VALUE_KI
     * @uses self::VALUE_KP
     * @uses self::VALUE_KR
     * @uses self::VALUE_KW
     * @uses self::VALUE_KG
     * @uses self::VALUE_LA
     * @uses self::VALUE_LV
     * @uses self::VALUE_LB
     * @uses self::VALUE_LS
     * @uses self::VALUE_LR
     * @uses self::VALUE_LY
     * @uses self::VALUE_LI
     * @uses self::VALUE_LT
     * @uses self::VALUE_LU
     * @uses self::VALUE_MO
     * @uses self::VALUE_MK
     * @uses self::VALUE_MG
     * @uses self::VALUE_MW
     * @uses self::VALUE_MY
     * @uses self::VALUE_MV
     * @uses self::VALUE_ML
     * @uses self::VALUE_MT
     * @uses self::VALUE_MH
     * @uses self::VALUE_MQ
     * @uses self::VALUE_MR
     * @uses self::VALUE_MU
     * @uses self::VALUE_YT
     * @uses self::VALUE_MX
     * @uses self::VALUE_FM
     * @uses self::VALUE_MD
     * @uses self::VALUE_MC
     * @uses self::VALUE_MN
     * @uses self::VALUE_MS
     * @uses self::VALUE_MA
     * @uses self::VALUE_MZ
     * @uses self::VALUE_MM
     * @uses self::VALUE_NA
     * @uses self::VALUE_NR
     * @uses self::VALUE_NP
     * @uses self::VALUE_NL
     * @uses self::VALUE_AN
     * @uses self::VALUE_NC
     * @uses self::VALUE_NZ
     * @uses self::VALUE_NI
     * @uses self::VALUE_NE
     * @uses self::VALUE_NG
     * @uses self::VALUE_NU
     * @uses self::VALUE_NF
     * @uses self::VALUE_MP
     * @uses self::VALUE_NO
     * @uses self::VALUE_OM
     * @uses self::VALUE_PK
     * @uses self::VALUE_PW
     * @uses self::VALUE_PS
     * @uses self::VALUE_PA
     * @uses self::VALUE_PG
     * @uses self::VALUE_PY
     * @uses self::VALUE_PE
     * @uses self::VALUE_PH
     * @uses self::VALUE_PN
     * @uses self::VALUE_PL
     * @uses self::VALUE_PT
     * @uses self::VALUE_PR
     * @uses self::VALUE_QA
     * @uses self::VALUE_RE
     * @uses self::VALUE_RO
     * @uses self::VALUE_RU
     * @uses self::VALUE_RW
     * @uses self::VALUE_SH
     * @uses self::VALUE_KN
     * @uses self::VALUE_LC
     * @uses self::VALUE_PM
     * @uses self::VALUE_VC
     * @uses self::VALUE_WS
     * @uses self::VALUE_SM
     * @uses self::VALUE_ST
     * @uses self::VALUE_SA
     * @uses self::VALUE_SN
     * @uses self::VALUE_CS
     * @uses self::VALUE_SC
     * @uses self::VALUE_SL
     * @uses self::VALUE_SG
     * @uses self::VALUE_SK
     * @uses self::VALUE_SI
     * @uses self::VALUE_SB
     * @uses self::VALUE_SO
     * @uses self::VALUE_ZA
     * @uses self::VALUE_GS
     * @uses self::VALUE_ES
     * @uses self::VALUE_LK
     * @uses self::VALUE_SD
     * @uses self::VALUE_SR
     * @uses self::VALUE_SJ
     * @uses self::VALUE_SZ
     * @uses self::VALUE_SE
     * @uses self::VALUE_CH
     * @uses self::VALUE_SY
     * @uses self::VALUE_TW
     * @uses self::VALUE_TJ
     * @uses self::VALUE_TZ
     * @uses self::VALUE_TH
     * @uses self::VALUE_TL
     * @uses self::VALUE_TG
     * @uses self::VALUE_TK
     * @uses self::VALUE_TO
     * @uses self::VALUE_TT
     * @uses self::VALUE_TN
     * @uses self::VALUE_TR
     * @uses self::VALUE_TM
     * @uses self::VALUE_TC
     * @uses self::VALUE_TV
     * @uses self::VALUE_UG
     * @uses self::VALUE_UA
     * @uses self::VALUE_AE
     * @uses self::VALUE_GB
     * @uses self::VALUE_US
     * @uses self::VALUE_UM
     * @uses self::VALUE_UY
     * @uses self::VALUE_UZ
     * @uses self::VALUE_VU
     * @uses self::VALUE_VA
     * @uses self::VALUE_VE
     * @uses self::VALUE_VN
     * @uses self::VALUE_VG
     * @uses self::VALUE_VI
     * @uses self::VALUE_WF
     * @uses self::VALUE_EH
     * @uses self::VALUE_YE
     * @uses self::VALUE_ZM
     * @uses self::VALUE_ZW
     * @uses self::VALUE_RS
     * @uses self::VALUE_IM
     * @uses self::VALUE_JE
     * @uses self::VALUE_TP
     * @uses self::VALUE_ME
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_AF,
            self::VALUE_AX,
            self::VALUE_AL,
            self::VALUE_DZ,
            self::VALUE_AS,
            self::VALUE_AD,
            self::VALUE_AO,
            self::VALUE_AI,
            self::VALUE_AQ,
            self::VALUE_AG,
            self::VALUE_AR,
            self::VALUE_AM,
            self::VALUE_AW,
            self::VALUE_AU,
            self::VALUE_AT,
            self::VALUE_AZ,
            self::VALUE_BS,
            self::VALUE_BH,
            self::VALUE_BD,
            self::VALUE_BB,
            self::VALUE_BY,
            self::VALUE_BE,
            self::VALUE_BZ,
            self::VALUE_BJ,
            self::VALUE_BM,
            self::VALUE_BT,
            self::VALUE_BO,
            self::VALUE_BA,
            self::VALUE_BW,
            self::VALUE_BV,
            self::VALUE_BR,
            self::VALUE_IO,
            self::VALUE_BN,
            self::VALUE_BG,
            self::VALUE_BF,
            self::VALUE_BI,
            self::VALUE_KH,
            self::VALUE_CM,
            self::VALUE_CA,
            self::VALUE_CV,
            self::VALUE_KY,
            self::VALUE_CF,
            self::VALUE_TD,
            self::VALUE_CL,
            self::VALUE_CN,
            self::VALUE_CX,
            self::VALUE_CC,
            self::VALUE_CO,
            self::VALUE_KM,
            self::VALUE_CG,
            self::VALUE_CD,
            self::VALUE_CK,
            self::VALUE_CR,
            self::VALUE_CI,
            self::VALUE_HR,
            self::VALUE_CU,
            self::VALUE_CY,
            self::VALUE_CZ,
            self::VALUE_DK,
            self::VALUE_DJ,
            self::VALUE_DM,
            self::VALUE_DO,
            self::VALUE_EC,
            self::VALUE_EG,
            self::VALUE_SV,
            self::VALUE_GQ,
            self::VALUE_ER,
            self::VALUE_EE,
            self::VALUE_ET,
            self::VALUE_FK,
            self::VALUE_FO,
            self::VALUE_FJ,
            self::VALUE_FI,
            self::VALUE_FR,
            self::VALUE_FX,
            self::VALUE_GF,
            self::VALUE_PF,
            self::VALUE_TF,
            self::VALUE_GA,
            self::VALUE_GM,
            self::VALUE_GE,
            self::VALUE_DE,
            self::VALUE_GH,
            self::VALUE_GI,
            self::VALUE_GR,
            self::VALUE_GL,
            self::VALUE_GD,
            self::VALUE_GP,
            self::VALUE_GU,
            self::VALUE_GT,
            self::VALUE_GN,
            self::VALUE_GW,
            self::VALUE_GY,
            self::VALUE_HT,
            self::VALUE_HM,
            self::VALUE_HN,
            self::VALUE_HK,
            self::VALUE_HU,
            self::VALUE_IS,
            self::VALUE_IN,
            self::VALUE_ID,
            self::VALUE_IR,
            self::VALUE_IQ,
            self::VALUE_IE,
            self::VALUE_IL,
            self::VALUE_IT,
            self::VALUE_JM,
            self::VALUE_JP,
            self::VALUE_JO,
            self::VALUE_KZ,
            self::VALUE_KE,
            self::VALUE_KI,
            self::VALUE_KP,
            self::VALUE_KR,
            self::VALUE_KW,
            self::VALUE_KG,
            self::VALUE_LA,
            self::VALUE_LV,
            self::VALUE_LB,
            self::VALUE_LS,
            self::VALUE_LR,
            self::VALUE_LY,
            self::VALUE_LI,
            self::VALUE_LT,
            self::VALUE_LU,
            self::VALUE_MO,
            self::VALUE_MK,
            self::VALUE_MG,
            self::VALUE_MW,
            self::VALUE_MY,
            self::VALUE_MV,
            self::VALUE_ML,
            self::VALUE_MT,
            self::VALUE_MH,
            self::VALUE_MQ,
            self::VALUE_MR,
            self::VALUE_MU,
            self::VALUE_YT,
            self::VALUE_MX,
            self::VALUE_FM,
            self::VALUE_MD,
            self::VALUE_MC,
            self::VALUE_MN,
            self::VALUE_MS,
            self::VALUE_MA,
            self::VALUE_MZ,
            self::VALUE_MM,
            self::VALUE_NA,
            self::VALUE_NR,
            self::VALUE_NP,
            self::VALUE_NL,
            self::VALUE_AN,
            self::VALUE_NC,
            self::VALUE_NZ,
            self::VALUE_NI,
            self::VALUE_NE,
            self::VALUE_NG,
            self::VALUE_NU,
            self::VALUE_NF,
            self::VALUE_MP,
            self::VALUE_NO,
            self::VALUE_OM,
            self::VALUE_PK,
            self::VALUE_PW,
            self::VALUE_PS,
            self::VALUE_PA,
            self::VALUE_PG,
            self::VALUE_PY,
            self::VALUE_PE,
            self::VALUE_PH,
            self::VALUE_PN,
            self::VALUE_PL,
            self::VALUE_PT,
            self::VALUE_PR,
            self::VALUE_QA,
            self::VALUE_RE,
            self::VALUE_RO,
            self::VALUE_RU,
            self::VALUE_RW,
            self::VALUE_SH,
            self::VALUE_KN,
            self::VALUE_LC,
            self::VALUE_PM,
            self::VALUE_VC,
            self::VALUE_WS,
            self::VALUE_SM,
            self::VALUE_ST,
            self::VALUE_SA,
            self::VALUE_SN,
            self::VALUE_CS,
            self::VALUE_SC,
            self::VALUE_SL,
            self::VALUE_SG,
            self::VALUE_SK,
            self::VALUE_SI,
            self::VALUE_SB,
            self::VALUE_SO,
            self::VALUE_ZA,
            self::VALUE_GS,
            self::VALUE_ES,
            self::VALUE_LK,
            self::VALUE_SD,
            self::VALUE_SR,
            self::VALUE_SJ,
            self::VALUE_SZ,
            self::VALUE_SE,
            self::VALUE_CH,
            self::VALUE_SY,
            self::VALUE_TW,
            self::VALUE_TJ,
            self::VALUE_TZ,
            self::VALUE_TH,
            self::VALUE_TL,
            self::VALUE_TG,
            self::VALUE_TK,
            self::VALUE_TO,
            self::VALUE_TT,
            self::VALUE_TN,
            self::VALUE_TR,
            self::VALUE_TM,
            self::VALUE_TC,
            self::VALUE_TV,
            self::VALUE_UG,
            self::VALUE_UA,
            self::VALUE_AE,
            self::VALUE_GB,
            self::VALUE_US,
            self::VALUE_UM,
            self::VALUE_UY,
            self::VALUE_UZ,
            self::VALUE_VU,
            self::VALUE_VA,
            self::VALUE_VE,
            self::VALUE_VN,
            self::VALUE_VG,
            self::VALUE_VI,
            self::VALUE_WF,
            self::VALUE_EH,
            self::VALUE_YE,
            self::VALUE_ZM,
            self::VALUE_ZW,
            self::VALUE_RS,
            self::VALUE_IM,
            self::VALUE_JE,
            self::VALUE_TP,
            self::VALUE_ME,
        ];
    }
}
