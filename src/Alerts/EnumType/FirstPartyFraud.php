<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts\EnumType;

use WsdlToPhp\PackageBase\AbstractStructEnumBase;

/**
 * This class stands for FirstPartyFraud EnumType
 *
 * @subpackage Enumerations
 */
class FirstPartyFraud extends AbstractStructEnumBase
{
    /**
     * Constant for value 'yes'
     *
     * @return string 'yes'
     */
    public const VALUE_YES = 'yes';
    /**
     * Constant for value 'no'
     *
     * @return string 'no'
     */
    public const VALUE_NO = 'no';

    /**
     * Return allowed values
     *
     * @return string[]
     * @uses self::VALUE_NO
     * @uses self::VALUE_YES
     */
    public static function getValidValues(): array
    {
        return [
            self::VALUE_YES,
            self::VALUE_NO,
        ];
    }
}
