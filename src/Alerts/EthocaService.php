<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts;

use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsRequest;
use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsResponse;
use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsUpdateRequest;
use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsUpdateResponse;
use Ratespecial\Ethoca\Alerts\StructType\EthocaAlertAcknowledgementRequest;
use Ratespecial\Ethoca\Alerts\StructType\EthocaAlertAcknowledgementResponse;
use Ratespecial\Ethoca\Alerts\StructType\EthocaRequestInterface;
use SoapFault;
use WsdlToPhp\PackageBase\AbstractSoapClientBase;

class EthocaService extends AbstractSoapClientBase
{
    protected string $username = '';
    protected string $password = '';

    public function setAuthentication(string $username, string $password): self
    {
        $this->username = $username;
        $this->password = $password;

        return $this;
    }

    protected function applyAuthenticationToRequest(EthocaRequestInterface $requestObj): void
    {
        if (empty($requestObj->getUsername())) {
            $requestObj->setUsername($this->username);
        }

        if (empty($requestObj->getPassword())) {
            $requestObj->setPassword($this->password);
        }
    }

    /**
     * Pull down new alerts (i.e., all those not previously received). It is possible to pull down up to 100* new alerts with one request
     * and additional requests will be required should any more alerts still need to be pulled down.
     *
     * @throws SoapFault
     */
    public function alerts(Ethoca360AlertsRequest $body): Ethoca360AlertsResponse
    {
        $this->applyAuthenticationToRequest($body);

        try {
            $this->setResult($resultEthoca360Alerts = $this->getSoapClient()->__soapCall('Ethoca360Alerts', [
                $body,
            ], [], [], $this->outputHeaders));

            return $resultEthoca360Alerts;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);

            throw $soapFault;
        }
    }

    /**
     * Update one or more alert outcomes and provide additional feedback.  A maximum of 25 AlertUpdates only are to be included in one
     * request to keep the response time to acceptable levels. More than that will result in an error response code of 413 indicating the
     * payload was too large.
     *
     * @throws SoapFault
     */
    public function alertUpdate(Ethoca360AlertsUpdateRequest $body): Ethoca360AlertsUpdateResponse
    {
        $this->applyAuthenticationToRequest($body);

        try {
            $this->setResult($resultEthoca360AlertsUpdate = $this->getSoapClient()->__soapCall('Ethoca360AlertsUpdate', [
                $body,
            ], [], [], $this->outputHeaders));

            return $resultEthoca360AlertsUpdate;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);

            throw $soapFault;
        }
    }

    /**
     * Allows the member to acknowledge one or more alerts. A maximum of 25 AlertAcknowledgements can be included in one request to keep
     * the response times at acceptable levels.
     *
     * @throws SoapFault
     */
    public function alertAcknowledgement(EthocaAlertAcknowledgementRequest $body): EthocaAlertAcknowledgementResponse
    {
        $this->applyAuthenticationToRequest($body);

        try {
            $this->setResult($resultEthocaAlertsAcknowledgement = $this->getSoapClient()->__soapCall('EthocaAlertsAcknowledgement', [
                $body,
            ], [], [], $this->outputHeaders));

            return $resultEthocaAlertsAcknowledgement;
        } catch (SoapFault $soapFault) {
            $this->saveLastError(__METHOD__, $soapFault);

            throw $soapFault;
        }
    }
}
