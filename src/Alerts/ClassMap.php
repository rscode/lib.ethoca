<?php

declare(strict_types=1);

namespace Ratespecial\Ethoca\Alerts;

/**
 * Class which returns the class map definition
 */
class ClassMap
{
    /**
     * Returns the mapping between the WSDL Structs and generated Structs' classes
     * This array is sent to the \SoapClient when calling the WS
     * @return string[]
     */
    final public static function get(): array
    {
        return [
            'ErrorsType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\ErrorsType',
            'ErrorType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\ErrorType',
            'Ethoca360AlertsRequestType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\Ethoca360AlertsRequest',
            'Ethoca360AlertsResponseType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\Ethoca360AlertsResponse',
            'AlertsType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertsType',
            'AlertType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertType',
            'Ethoca360AlertsUpdateRequestType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\Ethoca360AlertsUpdateRequest',
            'AlertUpdatesType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertUpdatesType',
            'AlertUpdateType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertUpdateType',
            'Ethoca360AlertsUpdateResponseType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\Ethoca360AlertsUpdateResponse',
            'AlertUpdateResponsesType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertUpdateResponsesType',
            'AlertUpdateResponseType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertUpdateResponseType',
            'EthocaAlertAcknowledgementRequestType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\EthocaAlertAcknowledgementRequest',
            'AlertAcknowledgementRequestType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\AlertAcknowledgementRequestType',
            'EthocaAlertAcknowledgementResponseType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\EthocaAlertAcknowledgementResponse',
            'EthocaAcknowledgementRequestType' => '\\Ratespecial\\Ethoca\\Alerts\\StructType\\EthocaAcknowledgementRequestType',
        ];
    }
}
