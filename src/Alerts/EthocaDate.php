<?php

namespace Ratespecial\Ethoca\Alerts;

class EthocaDate
{
    /**
     * Format Ethoca expects for their date field
     */
    public const DATE_FORMAT = 'Y-m-d\TH:i:s';

    /**
     * Interprets a date using strtotime and formats it to Ethoca's expectations.  Pass empty string to get current time.
     *
     * @param string $date
     * @return string
     */
    public static function format(string $date = ''): string
    {
        if (empty($date)) {
            return date(self::DATE_FORMAT);
        }

        return date(self::DATE_FORMAT, strtotime($date));
    }
}
