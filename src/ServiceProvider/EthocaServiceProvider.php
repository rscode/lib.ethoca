<?php

namespace Ratespecial\Ethoca\ServiceProvider;

use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;
use Ratespecial\Ethoca\Alerts\ClassMap;
use Ratespecial\Ethoca\Alerts\EthocaService;
use WsdlToPhp\PackageBase\SoapClientInterface;

/**
 * Laravel service provider.  Auto discovered
 */
class EthocaServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public const LIVE_SERVICE = 'ethoca-live';
    public const SANDBOX_SERVICE = 'ethoca-sandbox';

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/ethoca.php', 'ethoca');

        $this->registerEthocaService();
    }

    protected function registerEthocaService(): void
    {
        $this->app->bind(self::LIVE_SERVICE, function () {
            $options = [
                SoapClientInterface::WSDL_URL                => realpath(__DIR__ . '/../../wsdl/EthocaAlerts - Sandbox.wsdl'),
                SoapClientInterface::WSDL_CLASSMAP           => ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION           => 'https://services.ethocaweb.com/services/EthocaServices',

                // Ethoca docs recommend 140 seconds
                SoapClientInterface::WSDL_CONNECTION_TIMEOUT => 140,
            ];

            $s = new EthocaService($options);

            $config = $this->app['config']['ethoca'];
            $s->setAuthentication($config['username'], $config['password']);

            return $s;
        });

        $this->app->bind(self::SANDBOX_SERVICE, function () {
            $options = [
                SoapClientInterface::WSDL_URL                => realpath(__DIR__ . '/../../wsdl/EthocaAlerts - Sandbox.wsdl'),
                SoapClientInterface::WSDL_CLASSMAP           => ClassMap::get(),
                SoapClientInterface::WSDL_LOCATION           => 'https://sandbox.ethocaweb.com/axis2/services/EthocaServices',

                // Ethoca docs recommend 140 seconds
                SoapClientInterface::WSDL_CONNECTION_TIMEOUT => 140,
            ];

            $s = new EthocaService($options);

            $config = $this->app['config']['ethoca'];
            $s->setAuthentication($config['username'], $config['password']);

            return $s;
        });

        $this->app->bind(EthocaService::class, $this->app['config']['ethoca.env']);
    }

    public function provides(): array
    {
        return [
            EthocaService::class,
            self::LIVE_SERVICE,
            self::SANDBOX_SERVICE,
        ];
    }
}
