<?php

namespace Ratespecial\Ethoca\Webhook;

/**
 * Structure of the response to the Ethoca webhook.
 */
class EthocaAlertResponse
{
    /**
     * Ethoca generated unique ID for the alert
     *
     * @var string
     */
    public string $ethoca_id = '';
    /**
     * received = the transaction was updated successfully
     * retry = there was a temporary error processing this alert; this alert can be re-attempted by Ethoca in 15 minutes
     *
     * @var string received|retry
     */
    public string $status = 'received';
}
