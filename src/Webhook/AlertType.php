<?php

namespace Ratespecial\Ethoca\Webhook;

enum AlertType: string
{
    case ISSUER = 'issuer_alert';
    case FRAUD_REPORTER = 'fraudreporter_alert';
    case CUSTOMER_DISPUTE = 'customerdispute_alert';
}
