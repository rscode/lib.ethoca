<?php

namespace Ratespecial\Ethoca\Webhook;

class EthocaAlert
{
    public string $ethoca_id = '';
    public ?AlertType $type = null;
    public string $alert_timestamp = '';
    /**
     * Numeric age of the alert. It is the number of hours between the transaction date/time (authorisation date/time) and the
     * AlertTimestamp
     *
     * @var int
     */
    public int $age = 0;
    public string $issuer = '';
    public string $card_number = '';
    public string $card_bin = '';
    public string $card_last4 = '';
    public ?string $arn = null;
    /**
     * This is the authorisation date/time for the transaction. YYYY-MM-DDThh:mm:ss in the member’s time zone (e.g. in EST)
     *
     * @var string
     */
    public string $transaction_timestamp = '';
    public string $merchant_descriptor = '';
    /**
     * A unique ID for each Ethoca member. For partners receiving alerts on behalf of merchants then this will be the MemberID for the
     * merchant
     *
     * @var int
     */
    public int $member_id = 0;
    /**
     * Merchant Category Code – defines the industry to which the transaction belongs
     *
     * @var string|null
     */
    public ?string $mcc = null;
    public string $amount = '';
    public string $currency = '';
    public string $transaction_type = '';
    /**
     * @var string issuer|cardholder|not_available
     */
    public string $initiated_by = '';
    /**
     * @var string yes|no|not_available
     */
    public string $is_3d_secure = '';
    /**
     * This field indicates whether an alert was originated directly from an issuer, or from another source
     *
     * @var string|null
     */
    public ?string $source = null;
    public ?string $auth_code = null;
    public string $merchant_member_name = '';
    public ?string $transaction_id = null;
    public ?string $chargeback_reason_code = null;
    public ?string $chargeback_amount = null;
    public ?string $chargeback_currency = null;
}
