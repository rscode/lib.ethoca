<?php

namespace Tests\Ratespecial\Ethoca;

use PHPUnit\Framework\TestCase;
use Ratespecial\Ethoca\Alerts\ClassMap;
use Ratespecial\Ethoca\Alerts\EnumType\Outcome;
use Ratespecial\Ethoca\Alerts\EnumType\Refunded;
use Ratespecial\Ethoca\Alerts\EnumType\Status;
use Ratespecial\Ethoca\Alerts\EthocaDate;
use Ratespecial\Ethoca\Alerts\EthocaService;
use Ratespecial\Ethoca\Alerts\StructType\AlertAcknowledgementRequestType;
use Ratespecial\Ethoca\Alerts\StructType\AlertUpdatesType;
use Ratespecial\Ethoca\Alerts\StructType\AlertUpdateType;
use Ratespecial\Ethoca\Alerts\StructType\Ethoca360AlertsUpdateRequest;
use Ratespecial\Ethoca\Alerts\StructType\EthocaAcknowledgementRequestType;
use Ratespecial\Ethoca\Alerts\StructType\EthocaAlertAcknowledgementRequest;
use WsdlToPhp\PackageBase\SoapClientInterface;

class LiveTest extends TestCase
{
    public function testUpdate(): void
    {
        $s = $this->buildService();

        $upd = new AlertUpdatesType([
            new AlertUpdateType(
                ethocaID: 'AA1K4MD0YT8HSHPV1I7IB4P63',
                outcome: Outcome::VALUE_ACCOUNT_SUSPENDED,
                refunded: Refunded::VALUE_NOT_SETTLED,
                actionTimestamp: EthocaDate::format(),
                comments: 'whoopsie',
            ),
//            new AlertUpdateType(
//                ethocaID: 'AA1K4MD0YT8HSHPV1I7IB4P64',
//                outcome: Outcome::VALUE_ACCOUNT_SUSPENDED,
//                refunded: Refunded::VALUE_NOT_SETTLED,
//                actionTimestamp: EthocaDate::format(),
//                comments: 'whoopsie',
//            ),
        ]);
        $req = new Ethoca360AlertsUpdateRequest();
        $req->setAlertUpdates($upd);

        $res = $s->alertUpdate($req);
        var_dump($res);
    }

    public function testAlertAcknowledgement(): void
    {
        $s = $this->buildService();

        $id = '9UK0KUESEQ1OD098TP3K3JZI4';

        $acknowledgements = new AlertAcknowledgementRequestType([
            new EthocaAcknowledgementRequestType(
                ethocaID: $id,
                status: Status::VALUE_SUCCESS
            )
        ]);
        $req              = new EthocaAlertAcknowledgementRequest($acknowledgements);

        $result = $s->alertAcknowledgement($req);

        var_dump($result);
    }

    private function buildService(): EthocaService
    {
        $options = [
            SoapClientInterface::WSDL_URL                => realpath(__DIR__ . '/../wsdl/EthocaAlerts - Sandbox.wsdl'),
            SoapClientInterface::WSDL_CLASSMAP           => ClassMap::get(),
            SoapClientInterface::WSDL_LOCATION           => 'https://sandbox.ethocaweb.com/axis2/services/EthocaServices',
            SoapClientInterface::WSDL_CONNECTION_TIMEOUT => 140,
        ];

        $s = new EthocaService($options);

        $s->setAuthentication(getenv('RATESPECIAL_ETHOCA_USERNAME'), getenv('RATESPECIAL_ETHOCA_PASSWORD'));

        return $s;
    }
}
