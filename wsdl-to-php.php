<?php

use WsdlToPhp\PackageGenerator\ConfigurationReader\GeneratorOptions;
use WsdlToPhp\PackageGenerator\Generator\Generator;

require 'vendor/autoload.php';

$options = GeneratorOptions::instance();
$options->setOrigin('wsdl/EthocaAlerts - Sandbox.wsdl')
    ->setDestination('src/Alerts')
    ->setStandalone(false)
    ->setGenerateTutorialFile(false)
    ->setNamespaceDictatesDirectories(false)
    ->setSrcDirname('')
    ->setNamespace('Ratespecial\Ethoca\Alerts');

$generator = new Generator($options);
$generator->generatePackage();
